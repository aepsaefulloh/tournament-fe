@extends('components.master')

@section('content')


<div id="layerslider" style="width: 1900px; height: 530px;">

    <div class="ls-slide">
        <img src="{{asset('assets')}}/images/bg1.jpg" class="ls-bg" alt="slideback">

        <img src="{{asset('assets')}}/images/mtb.png" class="ls-l" data-ls="parallaxlevel: 5;" alt="Image layer" style="top:15%; left:50%;">

        <img src="{{asset('assets')}}/images/mt.png" class="ls-l" data-ls="parallaxlevel: -1;" alt="Image layer" style="top:15%; left:50%;">

        <div class="ls-l slider-text text-center" style="left:50%; top:35%;">
            With arcane you will be able to <span>Create A Real Gaming Community</span> where users can create
            teams, fight matches and manage tournaments. What are you waiting for? Start today!
        </div>

        <a href="index.html" class="ls-l" data-ls="offsetxin: 0; offsetyin: 230; delayin: 500" style="top: 70%; left: 34%;"><img src="{{asset('assets')}}/images/tip1.png">
            <p class="text-center">CREATE TEAMS</p>
        </a>

        <a href="index.html" class="ls-l" data-ls="offsetxin: 0; offsetyin: 230; delayin: 1000" style="top: 70%; left: 50%;"><img src="{{asset('assets')}}/images/tip2.png">
            <p class="text-center">FIGHT MATCHES</p>
        </a>

        <a href="index.html" class="ls-l" data-ls="offsetxin: 0; offsetyin: 230; delayin: 1500" style="top: 70%; left: 66%;"><img src="{{asset('assets')}}/images/tip3.png">
            <p class="text-center">MANAGE TOURNAMENTS</p>
        </a>

        <img src="{{asset('assets')}}/images/dividers.png" class="ls-l" data-ls="fadein: true" alt="Image layer" style="top:70%; left:50%;">
    </div>


</div>

@include('components.include.news-ticker')


<!-- page content wrapper -->
<section class="content-wrapper container-fluid no-padding">

    <div class="container content-slider" id="owl-demo">
        @foreach($content as $slideShow)
        <div class="item">
            <a href="{{ url('/article-detail').'/'.$slideShow['ID'].'/'.Helper::url_slug($slideShow['TITLE']) }}">
                <img src="{{$slideShow['IMAGE']}}" alt="cs1">
                <div class="content-slider-rating">
                    <b class="role-color"><i class="fa fa-trophy"></i>4.5</b>/5
                </div>
                <div class="content-slider-text">
                    <span class="category-text rpg role">RPG</span>
                    <h3>{{$slideShow['TITLE']}}</h3>
                    <span class="meta-text">by admin</span>
                </div>
            </a>
        </div>
        @endforeach
    </div>

    <!-- columns wrapper -->
    <section class="container main">

        <!-- left column -->
        <div class="col-lg-3 left-column">

            <!-- content title -->
            <div class="main-content-title">
                <h3><i class="fa fa-newspaper-o"></i> popular posts</h3>
            </div>

            @foreach($popular_post as $item)
            <!-- single post -->
            <div class="single-post">
                <div class="thumbnail-wrapper">
                    <a href="{{ url('/article-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}" class="single-post-img"><img src="{{$item['IMAGE']}}" alt="{{$item['TITLE']}}"></a>
                    <a href="javascript:void(0)" class="category-link">RPG</a>
                    <span class="post-rating"><b><i class="fa fa-trophy"></i>5</b>/5</span>
                </div>
                <h4 class="news-title"><a href="{{ url('/article-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}">{{$item['TITLE']}}</a></h4>
                <p class="post-meta">
                    <img src="{{asset('assets')}}/images/11-1.jpg" alt="author"> by <a href="blog-style.html">{{$item['CREATE_BY']}}</a>
                    {{ Date('d-M-Y', strtotime($item['CREATE_TIMESTAMP'])) }},
                    2015 with <a href="{{ url('/article-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}">3 <i class="fa fa-comments-o"></i></a>
                </p>
                <p class="post-text">{{$item['SUBTITLE']}}</p>
            </div>
            @endforeach

        </div>

        <!-- middle column -->
        <div class="col-lg-5 mid-column">

            <!-- content title -->
            <div class="main-content-title">
                <h3><i class="fa fa-crosshairs"></i> latest matches </h3>
            </div>

            <!-- matches -->
            <div class="tab-content-wrapper">
                <ul class="nav nav-tabs matches-tab-wrapper">
                    <li class="active"><a data-toggle="tab" href="#h">ALL</a></li>
                    <!-- <li><a data-toggle="tab" href="#m1">CS:GO</a></li>
                    <li><a data-toggle="tab" href="#m2">LOL</a></li> -->
                </ul>

                <div class="tab-content">
                    <div id="h" class="tab-pane fade in active">

                        <!-- matches list -->
                        <ul class="match-list">
                            <!-- single match -->
                            @foreach($match as $key => $result_match)
                            <li class="single-match-wrapper">
                                <a href="{{url('/match-detail')}}">
                                    <span class="result {{($key % 2) == 0 ? 'lose':'win'}}">{{rand(10, 20).':'.rand(10, 19)}}</span>
                                    <div class="single-match">
                                        <img src="{{asset('assets')}}/images/qN1JFnkg-25x25.png">
                                        <span class="vs">vs</span>
                                        <img src="{{asset('assets')}}/images/cloud9-counterstrike-wallpaper-25x25.jpg">
                                    </div>
                                    <span class="date">{{$result_match['NAME_PLAYER1']}}:{{$result_match['NAME_PLAYER2']}} - {{$result_match['MATCHDATE']}}</span>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

            <!-- content title -->
            <div class="main-content-title">
                <h3><i class="fa fa-newspaper-o"></i> gaming news </h3>
            </div>

            <!-- single post news -->
            <div class="single-post">
                <div class="thumbnail-wrapper">
                    <a href="single-post.html" class="single-post-img"><img src="{{asset('assets')}}/images/9-305x200.jpg" alt="thumb"></a>
                    <a href="blog-style.html" class="category-link racing">Racing</a>
                    <span class="post-rating"><b class="racing-color"><i class="fa fa-trophy"></i>5</b>/5</span>
                </div>
                <h4 class="news-title"><a href="single-post.html">Aenean sed interdum quammus laoree...</a></h4>
                <p class="post-meta">
                    <img src="{{asset('assets')}}/images/11-1.jpg" alt="author"> by <a href="blog-style.html">admin</a>
                    on October 31,
                    2015 with <a href="single-post.html">3 <i class="fa fa-comments-o"></i></a>
                </p>
                <p class="post-text">Sed commodo, libero ut dignissim imperdiet, lorem nibh bibendum nisi, vel
                    blandit est eros sit amet elit. Mauris nec arcu vel tellus aliquam congue. Mauris fermentum sem
                    ut tortor ultricies dictum. Praesent at porttitor mauris. Curabitur pulvinar suscipit tortor
                    venenatis…</p>
            </div>

            <div class="single-post post-small racing">
                <div class="post-small-bg-wrapper">
                    <h4 class="news-title">
                        <a href="blog-style.html" class="category-link">racing</a>
                        <a href="single-post.html" class="post-title-link">Disque vel augue sit amet diam lobortis
                            posuere</a>
                    </h4>
                    <p class="post-meta">by <a href="blog-style.html">admin</a> on October 31, 2015 with <a href="single-post.html">3 <i class="fa fa-comments-o"></i></a></p>
                    <p class="post-text">Sed commodo, libero ut dignissim imperdiet, lorem nibh bibendum nisi, vel
                        blandit est eros sit amet ...</p>
                </div>
            </div>
        </div>

        <!-- right column -->
        <div class="col-lg-4 right-column">
            <!-- tournment slider -->
            <div id="atn-sidebar-carousel" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">

                    <!-- single tournament -->
                    <div class="item active carousel-tournament">
                        <div class="single-tournament">
                            <div class="carousel-tournament-info">
                                <a href="single-tournament.html">
                                    <h4>esl 2016</h4>
                                </a>
                                <div class="tournament-image">
                                    <a href="single-tournament.html"><img src="{{asset('assets')}}/images/cs-85x116.jpg"></a>
                                </div>
                                <div class="tournament-text">
                                    <p><span>Location: </span>Paris, France</p>
                                    <br>
                                    <p><span>Maximum participants:</span> 100 teams</p>
                                    <p><span>Current participants:</span> 4 teams</p>
                                    <br>
                                    <p class="tournament-prize"><span>Prize:</span> $5000, $2500, $1000, CS:GO
                                        T-Shirt</p>
                                </div>
                            </div>
                            <div class="tournament-join">
                                <a href="single-tournament.html">
                                    <i class="fa fa-trophy"></i> Join!
                                </a>
                            </div>
                            <div class="tournament-date">
                                <p>May 5, 2016, <span>9:00 pm</span></p>
                            </div>
                        </div>
                    </div>

                    <!-- single tournament -->
                    <div class="item carousel-tournament">
                        <div class="single-tournament">
                            <div class="carousel-tournament-info">
                                <a href="single-tournament.html">
                                    <h4>LOL RANDOMZ @ COMIC CON</h4>
                                </a>
                                <div class="tournament-image">
                                    <a href="single-tournament.html"><img src="{{asset('assets')}}/images/lol-85x116.jpg"></a>
                                </div>
                                <div class="tournament-text">
                                    <p><span>Location: </span>Server: 99.168.0.1</p>
                                    <br>
                                    <p><span>Maximum participants:</span> 128 teams</p>
                                    <p><span>Current participants:</span> 4 teams</p>
                                    <br>
                                    <p class="tournament-prize"><span>Prize:</span> $5000, $2500, $1000, CS:GO
                                        T-Shirt</p>
                                </div>
                            </div>
                            <div class="tournament-join">
                                <a href="single-tournament.html">
                                    <i class="fa fa-trophy"></i> Join!
                                </a>
                            </div>
                            <div class="tournament-date">
                                <p>May 5, 2016, <span>9:00 pm</span></p>
                            </div>
                        </div>
                    </div>

                    <!-- single tournament -->
                    <div class="item carousel-tournament">
                        <div class="single-tournament">
                            <div class="carousel-tournament-info">
                                <a href="single-tournament.html">
                                    <h4>ELECTRONIC SPORTS WORLD CUP 2018</h4>
                                </a>
                                <div class="tournament-image">
                                    <a href="single-tournament.html"><img src="{{asset('assets')}}/images/dota-85x116.jpg"></a>
                                </div>
                                <div class="tournament-text">
                                    <p><span>Location: </span>Paris, France</p>
                                    <br>
                                    <p><span>Maximum participants:</span> 48 teams</p>
                                    <p><span>Current participants:</span> 4 teams</p>
                                    <br>
                                    <p class="tournament-prize"><span>Prize:</span> $5000, $2500, $1000, CS:GO
                                        T-Shirt</p>
                                </div>
                            </div>
                            <div class="tournament-join">
                                <a href="single-tournament.html">
                                    <i class="fa fa-trophy"></i> Join!
                                </a>
                            </div>
                            <div class="tournament-date">
                                <p>May 5, 2016, <span>9:00 pm</span></p>
                            </div>
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#atn-sidebar-carousel" role="button" data-slide="prev">
                        <span class="fa fa-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#atn-sidebar-carousel" role="button" data-slide="next">
                        <span class="fa fa-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <!-- content title -->
            <!-- <div class="main-content-title">
                <h3><i class="fa fa-rss"></i> share the love</h3>
            </div> -->

            <!-- social networks share -->
            <!-- <ul class="share-links">
                <li class="single-share-link"><a href="index.html"><i class="fa fa-rss"></i></a></li>
                <li class="single-share-link"><a href="index.html"><i class="fa fa-dribbble"></i></a></li>
                <li class="single-share-link"><a href="index.html"><i class="fa fa-vimeo-square"></i></a></li>
                <li class="single-share-link"><a href="index.html"><i class="fa fa-youtube"></i></a></li>
                <li class="single-share-link"><a href="index.html"><i class="fa fa-twitch"></i></a></li>
                <li class="single-share-link"><a href="index.html"><i class="fa fa-steam"></i></a></li>
                <li class="single-share-link"><a href="index.html"><i class="fa fa-pinterest"></i></a></li>
                <li class="single-share-link"><a href="index.html"><i class="fa fa-google-plus"></i></a></li>
                <li class="single-share-link"><a href="index.html"><i class="fa fa-twitter"></i></a></li>
                <li class="single-share-link"><a href="index.html"><i class="fa fa-facebook"></i></a></li>
            </ul> -->

            <!-- content title -->
            <div class="main-content-title" style="margin-bottom: 8%">
                <h3 class="next-match-title"> <a href="{{url('tournament-upcoming')}}" class="text-primary"> Show All Tournament</a>
                </h3>
            </div>
            <div class="main-content-title">
                <h3 class="next-match-title"> next matches</h3>
            </div>

            <!-- next match slider -->
            <div id="sidebar-carousel" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    @foreach($match as $key => $newMatch)
                    <!-- single match -->
                    <div class="item {{($key == 0) ? 'active': ''}}">
                        <div class="next-match-wrap">
                            <div class="team-image">
                                <img src="{{asset('assets')}}/images/logo-helmet-150x124.jpg">
                            </div>
                            <div class="team-image">
                                <img src="{{asset('assets')}}/images/cloud9-counterstrike-wallpaper-150x124.jpg">
                            </div>
                            <div class="clearfix"></div>
                            <div class="team-names">
                                <div class="home-team">
                                    <span>{{$newMatch['NAME_PLAYER1']}}</span>
                                </div>
                                <div class="versus">
                                    VS
                                </div>
                                <div class="away-team">
                                    <span>{{$newMatch['NAME_PLAYER2']}}</span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="match-date">
                                5 vs 5 - November 22, 2019 <span>11:45 am</span>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#sidebar-carousel" role="button" data-slide="prev">
                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#sidebar-carousel" role="button" data-slide="next">
                    <span class="fa fa-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            <div class="main-content-title">
                <h3><i class="fa fa-group"></i> teams</h3>
            </div>
            @foreach($team as $allTeams)
            <!-- teams list -->
            <ul class="teams-list">

                <!-- single team -->
                <li class="single-team">
                    <div class="team-avatar">
                        <a href="{{url('/team-detail/'.$allTeams['ID'])}}"><img src="{{$allTeams['LOGO']}}" alt="teamAvatar" class="avarar"></a>
                    </div>
                    <div class="team-text">
                        <a href="{{url('/team-detail/'.$allTeams['ID'])}}">{{$allTeams['TEAM_NAME']}}</a>
                        <span class="team-member">{{$allTeams['total_member']}}</span>
                    </div>
                    <div class="clearfix"></div>
                </li>

            </ul>
            @endforeach
            <!-- content title -->
            <!-- <div class="main-content-title">
                <h3><i class="fa fa-newspaper-o"></i> latest shooters</h3>
            </div> -->

            <!-- review post list -->
            <!-- <ul class="sidebar-review"> -->

            <!-- single review -->
            <!-- <li class="single-review">
                    <a href="single-post.html">
                        <div class="img">
                            <img alt="img" src="{{asset('assets')}}/images/26-368x148.jpg">
                            <span class="overlay-link"></span>
                        </div>
                    </a>

                    <div class="review-text">
                        <a href="single-post.html" class="review-date"><span><i class="fa fa-calendar"></i> October
                                31, 2015</span></a>
                        <a href="single-post.html" class="review-title">Aenean sed interdum quammus laoreet posuere
                            pharet</a>
                        <div class="review-rating"><b class="role-color"><i class="fa fa-trophy"></i> 4</b>/5</div>
                    </div>

                    <div class="clearfix"></div> -->
            <!-- </li> -->

            <!-- </ul> -->
        </div>
    </section>

    <!-- video section -->
    <section class="video-section no-padding">
        <iframe class="inner" frameborder="0" allowfullscreen="1" title="YouTube video player" width="100%" height="50%" src="https://www.youtube.com/embed/Gp6xh4hfN_4?playlist=Gp6xh4hfN_4&amp;iv_load_policy=3&amp;enablejsapi=1&amp;disablekb=1&amp;autoplay=1&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;loop=1&amp;wmode=transparent&amp;origin=http%3A%2F%2Fskywarriorthemes.com&amp;widgetid=1" id="widget2"></iframe>
        <div class="video-text-wrapper">
            <img src="{{asset('assets')}}/images/cs.png" alt="cs" width="522" height="78">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec dui vitae massa tincidunt aliquam.
                Suspendisse potenti. Donec tincidunt tellus erat, at tempor elit ultrices eget. Aliquam a est eu
                dolor aliquam congue. Ut facilisis dapibus nunc vitae mattis.</p>
            <a class="button-medium" href="index.html" target="_blank">Download now</a>
        </div>
    </section>


    <!-- around the net and teams -->
    <section class="around-the-net container">
        <div class="col-lg-12">
            <div class="main-content-title">
                <h3><i class="fa fa-newspaper-o"></i> around the net</h3>
            </div>

            <!-- around-the-net-tab -->
            <div class="tab-content-wrapper">
                <ul class="nav nav-tabs matches-tab-wrapper atn-tab">
                    <li class="active"><a data-toggle="tab" href="#atn-home">racing</a></li>
                    <li><a data-toggle="tab" href="#atn-menu1">RPG</a></li>
                    <li><a data-toggle="tab" href="#atn-menu2">shooters</a></li>
                </ul>
                <div class="tab-content">

                    <!-- single tab -->
                    <div id="atn-home" class="tab-pane fade in active">

                        <!-- single post -->
                        <div class="single-post">
                            <div class="thumbnail-wrapper">
                                <a href="single-post.html" class="single-post-img"><img src="{{asset('assets')}}/images/9-422x300.jpg" alt="thumb"></a>
                                <a href="blog-style.html" class="category-link racing">Racing</a>
                                <span class="post-rating"><b class="racing-color"><i class="fa fa-trophy"></i>5</b>/5</span>
                            </div>
                            <h4 class="news-title"><a href="single-post.html">Aenean sed interdum quammus
                                    laoree...</a></h4>
                            <p class="post-meta">
                                <img src="{{asset('assets')}}/images/11-1.jpg" alt="author"> by <a href=".blog-style.html">admin</a> on
                                October 31, 2015 with <a href="single-post.html">3 <i class="fa fa-comments-o"></i></a>
                            </p>
                            <p class="post-text">Sed commodo, libero ut dignissim imperdiet, lorem nibh bibendum
                                nisi, vel blandit est eros sit amet elit. Mauris nec arcu vel tellus aliquam congue.
                                Mauris fermentum sem ut tortor ultricies dictum. Praesent at porttitor mauris.
                                Curabitur pulvinar suscipit tortor venenatis…</p>
                        </div>

                        <!-- single small post -->
                        <div class="single-post post-small racing">
                            <div class="post-small-bg-wrapper">
                                <div class="thumbnail-wrapper">
                                    <a href="single-post.html" class="single-post-img"><img src="{{asset('assets')}}/images/30-120x95.jpg" alt="thumb"></a>
                                </div>
                                <h4 class="news-title">
                                    <a href="blog-style.html" class="category-link">racing</a>
                                    <a href="single-post.html" class="post-title-link">Disque vel augue sit amet
                                        diam lobortis posuere</a>
                                </h4>
                                <p class="post-meta">by <a href="blog-style.html">admin</a> on October 31, 2015 with
                                    <a href="single-post.html">3 <i class="fa fa-comments-o"></i></a>
                                </p>
                            </div>
                        </div>

                        <!-- single small post -->
                        <div class="single-post post-small racing">
                            <div class="post-small-bg-wrapper">
                                <div class="thumbnail-wrapper">
                                    <a href="single-post.html" class="single-post-img"><img src="{{asset('assets')}}/images/25-120x95.jpg" alt="thumb"></a>
                                </div>
                                <h4 class="news-title">
                                    <a href="blog-style.html" class="category-link">racing</a>
                                    <a href="single-post.html" class="post-title-link">Tentum a congue id congue sit
                                        amet era</a>
                                </h4>
                                <p class="post-meta">by <a href="blog-style.html">admin</a> on October 31, 2015 with
                                    <a href="single-post.html">3 <i class="fa fa-comments-o"></i></a>
                                </p>
                            </div>
                        </div>

                        <!-- single small post -->
                        <div class="single-post post-small racing">
                            <div class="post-small-bg-wrapper">
                                <div class="thumbnail-wrapper">
                                    <a href="single-post.html" class="single-post-img"><img src="{{asset('assets')}}/images/21-120x95.jpg" alt="thumb"></a>
                                </div>
                                <h4 class="news-title">
                                    <a href="blog-style.html" class="category-link">racing</a>
                                    <a href="single-post.html" class="post-title-link">Duis interdum quam id risus
                                        sollicitudin vel commod</a>
                                </h4>
                                <p class="post-meta">by <a href="b">admin</a> on October 31, 2015 with <a href="single-post.html">3 <i class="fa fa-comments-o"></i></a></p>
                            </div>
                        </div>

                        <!-- single small post -->
                        <div class="single-post post-small racing">
                            <div class="post-small-bg-wrapper">
                                <div class="thumbnail-wrapper">
                                    <a href="single-post.html" class="single-post-img"><img src="{{asset('assets')}}/images/6-1-120x95.jpg" alt="thumb"></a>
                                </div>
                                <h4 class="news-title">
                                    <a href="blog-style.html" class="category-link">racing</a>
                                    <a href="single-post.html" class="post-title-link">Morbi vel ipsum vel augue
                                        mattis ultricies non et mauris</a>
                                </h4>
                                <p class="post-meta">by <a href="blog-style.html">admin</a> on October 31, 2015 with
                                    <a href="single-post.html">3 <i class="fa fa-comments-o"></i></a>
                                </p>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                    <!-- single tab -->
                    <div id="atn-menu1" class="tab-pane fade in">

                        <!-- single post -->
                        <div class="single-post">
                            <div class="thumbnail-wrapper">
                                <a href="single-post.html" class="single-post-img"><img src="{{asset('assets')}}/images/29-350x230.jpg" alt="thumb"></a>
                                <a href="blog-style.html" class="category-link role">RPG</a>
                                <span class="post-rating"><b class="role-color"><i class="fa fa-trophy"></i>5</b>/5</span>
                            </div>
                            <h4 class="news-title"><a href="single-post.html">Mauris risus augue fermentum sit amet
                                    congue sit amet</a></h4>
                            <p class="post-meta">
                                <img src="{{asset('assets')}}/images/11-1.jpg" alt="author"> by <a href=".blog-style.html">admin</a> on
                                October 31, 2015 with <a href="single-post.html">3 <i class="fa fa-comments-o"></i></a>
                            </p>
                            <p class="post-text">Sed commodo, libero ut dignissim imperdiet, lorem nibh bibendum
                                nisi, vel blandit est eros sit amet elit. Mauris nec arcu vel tellus aliquam congue.
                                Mauris fermentum sem ut tortor ultricies dictum. Praesent at porttitor mauris.
                                Curabitur pulvinar suscipit tortor venenatis…</p>
                        </div>

                        <!-- single small post -->
                        <div class="single-post post-small role">
                            <div class="post-small-bg-wrapper">
                                <div class="thumbnail-wrapper">
                                    <a href="single-post.html" class="single-post-img"><img src="{{asset('assets')}}/images/30-120x95.jpg" alt="thumb"></a>
                                </div>
                                <h4 class="news-title">
                                    <a href="blog-style.html" class="category-link role">RPG</a>
                                    <a href="single-post.html" class="post-title-link">Quis nisi nec nulla malesuada
                                        scelerisque</a>
                                </h4>
                                <p class="post-meta">by <a href="blog-style.html">admin</a> on October 31, 2015 with
                                    <a href="single-post.html">3 <i class="fa fa-comments-o"></i></a>
                                </p>
                            </div>
                        </div>

                        <!-- single small post -->
                        <div class="single-post post-small role">
                            <div class="post-small-bg-wrapper">
                                <div class="thumbnail-wrapper">
                                    <a href="single-post.html" class="single-post-img"><img src="{{asset('assets')}}/images/25-120x95.jpg" alt="thumb"></a>
                                </div>
                                <h4 class="news-title">
                                    <a href="blog-style.html" class="category-link role">rpg</a>
                                    <a href="single-post.html" class="post-title-link">Etiam massa mauris fermentum
                                        a congue id</a>
                                </h4>
                                <p class="post-meta">by <a href="blog-style.html">admin</a> on October 31, 2015 with
                                    <a href="single-post.html">3 <i class="fa fa-comments-o"></i></a>
                                </p>
                            </div>
                        </div>

                        <!-- single small post -->
                        <div class="single-post post-small role">
                            <div class="post-small-bg-wrapper">
                                <div class="thumbnail-wrapper">
                                    <a href="single-post.html" class="single-post-img"><img src="{{asset('assets')}}/images/21-120x95.jpg" alt="thumb"></a>
                                </div>
                                <h4 class="news-title">
                                    <a href="blog-style.html" class="category-link role">rpg</a>
                                    <a href="single-post.html" class="post-title-link">Per inceptos himenaeos
                                        spontaneus areas tus vivos</a>
                                </h4>
                                <p class="post-meta">by <a href="b">admin</a> on October 31, 2015 with <a href="single-post.html">3 <i class="fa fa-comments-o"></i></a></p>
                            </div>
                        </div>

                        <!-- single small post -->
                        <div class="single-post post-small role">
                            <div class="post-small-bg-wrapper">
                                <div class="thumbnail-wrapper">
                                    <a href="single-post.html" class="single-post-img"><img src="{{asset('assets')}}/images/6-1-120x95.jpg" alt="thumb"></a>
                                </div>
                                <h4 class="news-title">
                                    <a href="blog-style.html" class="category-link role">rpg</a>
                                    <a href="single-post.html" class="post-title-link">Fermentum a etiam massa
                                        mauriscongue idos</a>
                                </h4>
                                <p class="post-meta">by <a href="blog-style.html">admin</a> on October 31, 2015 with
                                    <a href="single-post.html">3 <i class="fa fa-comments-o"></i></a>
                                </p>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                    <!-- single tab -->
                    <div id="atn-menu2" class="tab-pane fade in">

                        <!-- single post -->
                        <div class="single-post">
                            <div class="thumbnail-wrapper">
                                <a href="single-post.html" class="single-post-img"><img src="{{asset('assets')}}/images/26-422x300.jpg" alt="thumb"></a>
                                <a href="blog-style.html" class="category-link shooter">Shooter</a>
                                <span class="post-rating"><b class="shooter-color"><i class="fa fa-trophy"></i>5</b>/5</span>
                            </div>
                            <h4 class="news-title"><a href="single-post.html">Feugiat pharetra sapien ec vel turpis
                                    orc</a></h4>
                            <p class="post-meta">
                                <img src="{{asset('assets')}}/images/11-1.jpg" alt="author"> by <a href=".blog-style.html">admin</a> on
                                October 31, 2015 with <a href="single-post.html">3 <i class="fa fa-comments-o"></i></a>
                            </p>
                            <p class="post-text">Sed commodo, libero ut dignissim imperdiet, lorem nibh bibendum
                                nisi, vel blandit est eros sit amet elit. Mauris nec arcu vel tellus aliquam congue.
                                Mauris fermentum sem ut tortor ultricies dictum. Praesent at porttitor mauris.
                                Curabitur pulvinar suscipit tortor venenatis…</p>
                        </div>

                        <!-- single small post -->
                        <div class="single-post post-small shooter">
                            <div class="post-small-bg-wrapper">
                                <div class="thumbnail-wrapper">
                                    <a href="single-post.html" class="single-post-img"><img src="{{asset('assets')}}/images/30-120x95.jpg" alt="thumb"></a>
                                </div>
                                <h4 class="news-title">
                                    <a href="blog-style.html" class="category-link shooter">shooter</a>
                                    <a href="single-post.html" class="post-title-link">Pellentesque imperdiet
                                        finibus ipsum, sed porta</a>
                                </h4>
                                <p class="post-meta">by <a href="blog-style.html">admin</a> on October 31, 2015 with
                                    <a href="single-post.html">3 <i class="fa fa-comments-o"></i></a>
                                </p>
                            </div>
                        </div>

                        <!-- single small post -->
                        <div class="single-post post-small shooter">
                            <div class="post-small-bg-wrapper">
                                <div class="thumbnail-wrapper">
                                    <a href="single-post.html" class="single-post-img"><img src="{{asset('assets')}}/images/25-120x95.jpg" alt="thumb"></a>
                                </div>
                                <h4 class="news-title">
                                    <a href="blog-style.html" class="category-link shooter">shooter</a>
                                    <a href="single-post.html" class="post-title-link">Praesent nec tortor quis erat
                                        facilisis auctor</a>
                                </h4>
                                <p class="post-meta">by <a href="blog-style.html">admin</a> on October 31, 2015 with
                                    <a href="single-post.html">3 <i class="fa fa-comments-o"></i></a>
                                </p>
                            </div>
                        </div>

                        <!-- single small post -->
                        <div class="single-post post-small shooter">
                            <div class="post-small-bg-wrapper">
                                <div class="thumbnail-wrapper">
                                    <a href="single-post.html" class="single-post-img"><img src="{{asset('assets')}}/images/21-120x95.jpg" alt="thumb"></a>
                                </div>
                                <h4 class="news-title">
                                    <a href="blog-style.html" class="category-link shooter">shooter</a>
                                    <a href="single-post.html" class="post-title-link">Curabitur lorem mauris dictum
                                        et tempus</a>
                                </h4>
                                <p class="post-meta">by <a href="b">admin</a> on October 31, 2015 with <a href="single-post.html">3 <i class="fa fa-comments-o"></i></a></p>
                            </div>
                        </div>

                        <!-- single small post -->
                        <div class="single-post post-small shooter">
                            <div class="post-small-bg-wrapper">
                                <div class="thumbnail-wrapper">
                                    <a href="single-post.html" class="single-post-img"><img src="{{asset('assets')}}/images/6-1-120x95.jpg" alt="thumb"></a>
                                </div>
                                <h4 class="news-title">
                                    <a href="blog-style.html" class="category-link shooter">shooter</a>
                                    <a href="single-post.html" class="post-title-link">Disque vel augue sit amet
                                        diam lobortis posuere</a>
                                </h4>
                                <p class="post-meta">by <a href="blog-style.html">admin</a> on October 31, 2015 with
                                    <a href="single-post.html">3 <i class="fa fa-comments-o"></i></a>
                                </p>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-5 teams">

    </section>
</section>


<div class="container back-to-top-wrapper">
    <a href="#" class="back-to-top" id="scroll"></a>
</div>

@endsection

@push('scripts')
@endpush