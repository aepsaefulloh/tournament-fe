@extends('components.master')
{{-- Meta Tag HTML --}}
@section('link-cannonical', url()->current())
@section('meta-desc', strip_tags($content['SUMMARY']))
@section('meta-fb-title', $content['TITLE'])
@section('meta-fb-desc', strip_tags($content['SUMMARY']))
@section('meta-fb-image', $content['IMAGE'])
@section('meta-fb-url', url()->current())
@section('body_class', 'blog-style single-post-page')

{{-- END META TAG --}}
@section('content')

<!-- main slider -->
<div class="container-fluid no-padding">
    <div class="slider col-lg-12">
        <div class="category-link-wrapper"><a href="#">Racing</a></div>

        <h1>{{$content['TITLE']}}</h1>
        <strong><a href="index.html">Home</a> <span>/ {{$content['TITLE']}}</span></strong>
    </div>
</div>

@include('components.include.news-ticker')


<!-- page content wrapper -->
<section class="content-wrapper container-fluid no-padding">

    <section class="container main-content-wrapper">

        <div class="col-lg-8 no-padding">

            <div class="blog-post">
                <div class="blog-info">
                    <p class="post-meta">
                        <img src="{{asset('assets')}}/images/11-1.jpg" alt="author"> by <a href="javascript:void(0)">{{$content['CREATE_BY']}}</a> <i>|</i> <a href="blog-style.html">Strategy</a> <i>|</i> <a href="single-post.html">0 Comments</a>
                        <i>|</i> {{ Date('d-M-Y', strtotime($content['CREATE_TIMESTAMP'])) }}
                    </p>
                </div>

                <div class="blog-content">

                    {!! $content['CONTENT'] !!}
                </div>
            </div>
            <div class="clearfix"></div>

            <hr class="section-divider">

            <form id="commentform" method="post" action="{{route('article.post_comment')}}">
                @csrf
                <div class="form-section">
                    <input type="hidden" name="article_id" value="{{$id}}" />
                </div>
                <div class="form-section">
                    <textarea placeholder="Leave a comment..." id="comment" name="comment" cols="45" rows="8" tabindex="6"></textarea>
                </div>
                <div class="form-submit">
                    <input id="submit" name="submit" class="button-small button-green" type="submit" value="Submit comment" tabindex="7">
                </div>
            </form>
        </div>

        <div class="col-lg-4 sidebar">

            <form role="search" method="get" id="searchform" class="searchform" action="http://skywarriorthemes.com/arcane/">
                <div>
                    <input type="text" value="" name="s" id="s">
                    <input type="hidden" value="post" name="post_type">
                </div>
            </form>

            <!-- content title -->
            <div class="main-content-title">
                <h3><i class="fa fa-newspaper-o"></i> latest posts</h3>
            </div>

            <ul class="latest-posts">

                @foreach($latest_post as $item)

                <li class="single-post">
                    <a href="{{ url('/article-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}">
                        <div class="image-wrapper">
                            <img src="{{$item['IMAGE']}}">

                        </div>
                    </a>
                    <div class="latest-post-info">
                        <a href="{{ url('/article-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}">{{$item['TITLE']}}</a>
                        <div class="post-meta">
                            By&nbsp;<a href="javascript:void(0)">{{$item['CREATE_BY']}}</a> /<small><i class="fa fa-calendar"></i>
                                {{ Date('d-M-Y', strtotime($item['CREATE_TIMESTAMP'])) }}</small>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </li>
                @endforeach

            </ul>

            <!-- content title -->
            <div class="main-content-title">
                <h3>recent comments</h3>
            </div>

            <ul class="recent-comments">
                @foreach($comment as $user_comment)
                <li class="single-comment">
                    <div class="comments-avatar">
                        <a href="javascript:void(0)"><img src="{{asset('assets')}}/images/15-1.jpg"></a>
                    </div>
                    <div class="comment-details">
                        <a href="javascript:void(0)">{{$user_comment['USER']}} </a> <i>says:</i>
                        <p>{{$user_comment['COMMENT']}}</p>
                    </div>
                    <div class="clearfix"></div>

                </li>
                @endforeach
            </ul>

            <!-- content title -->
            <div class="main-content-title">
                <h3>gaming images</h3>
            </div>

            <div class="instagram-wrapper">

                <!-- single instagram image -->
                <a href="#">
                    <img src="{{asset('assets')}}/images/13715263_596813070502407_1646627604_n.jpg">
                </a>

                <!-- single instagram image -->
                <a href="#">
                    <img src="{{asset('assets')}}/images/13774591_1754065994811600_491978258_n.jpg">
                </a>

                <!-- single instagram image -->
                <a href="#">
                    <img src="{{asset('assets')}}/images/13706881_1118469068196774_1972721860_n.jpg">
                </a>

                <!-- single instagram image -->
                <a href="#">
                    <img src="{{asset('assets')}}/images/13739556_1158304420899822_1228846908_n.jpg">
                </a>

                <!-- single instagram image -->
                <a href="#">
                    <img src="{{asset('assets')}}/images/13694358_1042322892521584_1150638250_n.jpg">
                </a>

                <!-- single instagram image -->
                <a href="#">
                    <img src="{{asset('assets')}}/images/13767625_282321932129985_1522248215_n.jpg">
                </a>
            </div>
        </div>
    </section>


</section>


@endsection

@push('scripts')

@endpush