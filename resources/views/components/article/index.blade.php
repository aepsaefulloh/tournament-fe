@extends('components.master')
@section('body_class', 'blog-style blog-page')

@section('content')
<!-- page content wrapper -->

<!-- main slider -->
<div class="container-fluid no-padding">
    <div class="slider col-lg-12">
        <h1>Article</h1>
        <strong><a href="index.html">Home</a> <span>/ Article</span></strong>
    </div>
</div>
@include('components.include.news-ticker')
<section class="content-wrapper container-fluid no-padding">
    <div class="container main-wrapper">
        <div class="col-lg-8 main-content">
            <section class="latest-news">
                <!-- content title -->
                <div class="main-content-title">
                    <h3><i class="fa fa-newspaper-o"></i> latest news</h3>
                </div>

                <!-- single post news -->
                @foreach($content as $item)
                <div class="single-post">
                    <div class="thumbnail-wrapper">
                        <a href="{{ url('/article-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}"
                            class="single-post-img">
                            <img src="{{$item['IMAGE']}}" alt="{{strip_tags($item['TITLE'])}}"></a>

                    </div>
                    <div class="single-post-content">
                        <h2 class="news-title"><a
                                href="{{ url('/article-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}">{{$item['TITLE']}}</a>
                        </h2>
                        <p class="post-meta">
                            <img src="{{asset('assets')}}/images/11-1.jpg" alt="author"> by <a
                                href="javascript:void(0)">{{$item['CREATE_BY']}}</a> <i>|</i> <a
                                href="javascript:void(0)">{{$item['CATEGORY']}}</a> <i>|</i> <a
                                href="javascript:void(0)">0 Comments</a>
                            <i>|</i> {{ Date('d-M-Y', strtotime($item['CREATE_TIMESTAMP'])) }}
                        </p>
                        <p class="post-text">
                            {{$item['SUBTITLE']}}
                        </p>
                        <a class="button-medium"
                            href="{{ url('/article-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}"
                            target="_blank">read more</a>
                    </div>
                </div>
                @endforeach


            </section>

            <hr class="section-divider">

            <div class="pagination">
                <ul>
                    <li class="active"><a href="blog-right-sidebar.html">1</a></li>
                    <li><a href="blog-right-sidebar.html" class="inactive">2</a></li>
                    <li><a href="blog-right-sidebar.html">»</a></li>
                </ul>
            </div>
        </div>

        <div class="col-lg-4 sidebar">

            <form role="search" method="get" id="searchform" class="searchform"
                action="http://skywarriorthemes.com/arcane/">
                <div>
                    <input type="text" value="" name="s" id="s">
                    <input type="hidden" value="post" name="post_type">
                </div>
            </form>

            <!-- content title -->
            <div class="main-content-title">
                <h3><i class="fa fa-newspaper-o"></i> latest posts</h3>
            </div>

            <ul class="latest-posts">
                @foreach($latest_post as $item)
                <li class="single-post">
                    <a href="{{ url('/article-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}">
                        <div class="image-wrapper">
                            <img src="{{$item['IMAGE']}}">
                            <div class="post-rating">
                            </div>
                        </div>
                    </a>
                    <div class="latest-post-info">
                        <a
                            href="{{ url('/article-detail').'/'.$item['ID'].'/'.Helper::url_slug($item['TITLE']) }}">{{$item['TITLE']}}</a>
                        <div class="post-meta">
                            By&nbsp;<a href="javascript:void(0)">{{$item['CREATE_BY']}}</a> /<small><i
                                    class="fa fa-calendar"></i>{{ Date('d-M-Y', strtotime($item['CREATE_TIMESTAMP'])) }}</small>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </li>
                @endforeach

            </ul>

            <!-- content title -->
            <div class="main-content-title">
                <h3>recent comments</h3>
            </div>

            <ul class="recent-comments">
                <li class="single-comment">
                    <div class="comments-avatar">
                        <a href="profile.html"><img src="images/15-1.jpg"></a>
                    </div>
                    <div class="comment-details">
                        <a href="profile.html">Kyōsuke </a> <i>says:</i>
                        <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
                            Sed iacu...</p>
                    </div>
                    <div class="clearfix"></div>

                </li>

                <li class="single-comment">
                    <div class="comments-avatar">
                        <a href="profile.html"><img src="images/20-1.jpg"></a>
                    </div>
                    <div class="comment-details">
                        <a href="profile.html">FoxSlay </a> <i>says:</i>
                        <p>Mauris nec arcu vel tellus aliquam congue. Mauris fermentum sem ut tortor ultricies
                            dictum....</p>
                    </div>
                    <div class="clearfix"></div>

                </li>

                <li class="single-comment">
                    <div class="comments-avatar">
                        <a href="profile.html"><img src="images/12-1.jpg"></a>
                    </div>
                    <div class="comment-details">
                        <a href="profile.html">Raaaa </a> <i>says:</i>
                        <p>Nam metus tortor, ultricies a elementum volutpat, sodales dignissim lorem. Curabitur mollis,
                            enim id...</p>
                    </div>
                    <div class="clearfix"></div>

                </li>

                <li class="single-comment">
                    <div class="comments-avatar">
                        <a href="profile.html"><img src="images/19-1.jpg"></a>
                    </div>
                    <div class="comment-details">
                        <a href="profile.html">g0dspeed </a> <i>says:</i>
                        <p>Nam metus tortor, ultricies a elementum volutpat, sodales dignissim lorem. Curabitur mollis,
                            enim id...</p>
                    </div>
                    <div class="clearfix"></div>

                </li>

                <li class="single-comment">
                    <div class="comments-avatar">
                        <a href="profile.html"><img src="images/21-1.jpg"></a>
                    </div>
                    <div class="comment-details">
                        <a href="profile.html">Energet </a> <i>says:</i>
                        <p>Proin ex ipsum, malesuada at auctor id, mollis id urna....</p>
                    </div>
                    <div class="clearfix"></div>

                </li>
            </ul>

            <!-- content title -->
            <div class="main-content-title">
                <h3>gaming images</h3>
            </div>

            <div class="instagram-wrapper">

                <!-- single instagram image -->
                <a href="#">
                    <img src="images/13715263_596813070502407_1646627604_n.jpg">
                </a>

                <!-- single instagram image -->
                <a href="#">
                    <img src="images/13774591_1754065994811600_491978258_n.jpg">
                </a>

                <!-- single instagram image -->
                <a href="#">
                    <img src="images/13706881_1118469068196774_1972721860_n.jpg">
                </a>

                <!-- single instagram image -->
                <a href="#">
                    <img src="images/13739556_1158304420899822_1228846908_n.jpg">
                </a>

                <!-- single instagram image -->
                <a href="#">
                    <img src="images/13694358_1042322892521584_1150638250_n.jpg">
                </a>

                <!-- single instagram image -->
                <a href="#">
                    <img src="images/13767625_282321932129985_1522248215_n.jpg">
                </a>
            </div>
        </div>
    </div>
</section>

@endsection