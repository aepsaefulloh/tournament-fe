@extends('components.master')
@section('body_class', 'tournaments-page register')
@section('content')

    <!-- page content wrapper -->
    <section class="content-wrapper container-fluid no-padding">
        <section class="container">
            <div class="col-lg-7 col-md-12 register-form-wrapper">
                <!-- content title -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                
                @if (session('backendErrors'))
                    <div class="alert alert-danger">
                        <ul>
                            @foreach (session('backendErrors') as $error)
                                <li>{{ $error[0] }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (!Session::has('message') && !Session::has('email'))
                    <div class="main-content-title">
                        <h3>
                            Please enter your email address. You'll receive a link to create a new password via email.
                        </h3>
                    </div>
                @endif

                @if (Session::has('message') && Session::has('email'))
                    <div class="text-center">
                        <h3>
                            <strong>{{ Session::get('message') }}</strong>
                        </h3>
                        <p>
                            Success Send Email to {{ Session::get('email') }} <br>
                            Please Check Your Email To Reset Password
                        </p>
                    </div>
                @else
                    <form action="{{ route('forget') }}" method="post">
                        @csrf
                        <p>
                            <label>Email:</label>
                            <input type="text" name="email" class="register" required>
                        </p>

                        <p class="submit">
                            <button class="button-medium" type="submit">FORGET PASSWORD</button>
                        </p>

                    </form>
                @endif
            </div>
        </section>
    </section>

@endsection
@push('scripts')
@endpush
