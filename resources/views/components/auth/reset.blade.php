@extends('components.master')
@section('body_class', 'tournaments-page register')
@section('content')

    <!-- page content wrapper -->
    <section class="content-wrapper container-fluid no-padding">
        <section class="container">
            <div class="col-lg-7 col-md-12 register-form-wrapper">
                <!-- content title -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                
                @if (session('backendErrors'))
                    <div class="alert alert-danger">
                        <ul>
                            @foreach (session('backendErrors') as $error)
                                <li>{{ $error[0] }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (!Session::has('message'))
                    <div class="main-content-title">
                        <h3>
                            Please enter your new password
                        </h3>
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="text-center">
                        <h3>
                            <strong>{{ Session::get('message') }}</strong>
                        </h3>
                        <p>
                            Success Reset Password<br>
                            Please Login With Your New Password
                        </p>
                    </div>
                @else
                    <form action="{{ route('reset') }}" method="post">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        <p>
                            <label>Email:</label>
                            <input type="text" value="{{ $email }}" class="register" disabled>
                        </p>
                        
                        <p>
                            <label>Password:</label>
                            <input type="password" name="password" class="register" required>
                        </p>
                        
                        <p>
                            <label>Confirm Password:</label>
                            <input type="password" name="password_confirmation" class="register" required>
                        </p>

                        <p class="submit">
                            <button class="button-medium" type="submit">RESET PASSWORD</button>
                        </p>

                    </form>
                @endif
            </div>
        </section>
    </section>

@endsection
@push('scripts')
@endpush
