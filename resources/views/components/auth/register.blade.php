@extends('components.master')
@section('body_class', 'tournaments-page register')

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')

    <!-- main slider -->
    <div class="container-fluid no-padding">
        <div class="slider col-lg-12">
            <h1>Register</h1>
            <strong><a href="index.html">Home</a> <span>/ Register</span></strong>
        </div>
    </div>

    @include('components.include.news-ticker')

    <!-- page content wrapper -->
    <section class="content-wrapper container-fluid no-padding">
        <section class="container">
            <div class="col-lg-7 col-md-12 register-form-wrapper">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('backendErrors'))
                    <div class="alert alert-danger">
                        <ul>
                            @foreach (session('backendErrors') as $error)
                                <li>{{ $error[0] }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <!-- content title -->
                <div class="main-content-title">
                    <h3>JOIN ARCANE TODAY FOR FREE!</h3>
                </div>

                <form action="{{ route('register') }}" method="post">
                    @csrf
                    <p>
                        <label>Full Name:</label>
                        <input type="text" name="fullname" class="register" required>
                    </p>
                    <p>
                        <label>Username:</label>
                        <input type="text" name="username" class="register"  pattern="[^' ']+" title="Username Cannot Using Space" required>
                    </p>
                    <p>
                        <label>Email:</label>
                        <input type="email" name="email" class="register" required>
                    </p>
                    <p>
                        <label>Password:</label>
                        <input type="password" name="password" class="register" required>
                    </p>

                    <p>
                        <label>City:</label>
                        <select id="city" name="city_id" required>
                            @foreach ($city as $item)
                                <option value="{{ $item['ID'] }}">{{ $item['CITY'] }}</option>
                            @endforeach
                        </select>
                    </p>

                    <p class="checkbox-reg">
                        <label>
                            <input type="checkbox" name="agree" id="signupAgree" required>
                            <span for="signupAgree">I certify that I'm over 18 and I agree to the<a href="#"
                                    target="_blank">Terms &amp; Conditions!</a></span>
                        </label>
                    </p>

                    <p class="submit">
                        <button type="submit" class="button-medium">SIGN UP TODAY</button>
                    </p>

                </form>
            </div>
        </section>
    </section>


@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#city").select2();
        });
    </script>
@endpush
