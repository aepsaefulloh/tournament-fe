@extends('components.master')
{{-- Meta Tag HTML --}}
@section('link-cannonical', url()->current())
@section('meta-fb-url', url()->current())
@section('body_class', 'blog-style single-post-page')

{{-- END META TAG --}}
@section('content')
<!-- news ticker -->
<div class="container-fluid ticker">
    <div class="container">
        <div class="ticker-title">
            <i class="fa fa-bullhorn"></i><span>&nbsp;BREAKING NEWS</span>
        </div>
        <div class="tickercontainer">
            <div class="mask">
                <ul id="webticker" class="newsticker">
                    <li class="ticker-spacer"></li>
                    <li>This List Item will scroll infintely</li>
                    <li>And this one will follow it</li>
                    <li class="last">Finally when it goes out of screen, it will queue again at the end</li>
                </ul>
                <span class="tickeroverlay-left">&nbsp;</span>
                <span class="tickeroverlay-right">&nbsp;</span>
            </div>
        </div>
        <div class="search-top">
            <form method="get" id="sform" action="http://skywarriorthemes.com/arcane/">
                <input type="search" placeholder="Search for..." autocomplete="off" name="s" style="width: 3px; padding: 10px 20px 8px; border: 0px solid transparent; cursor: pointer; color: rgb(0, 0, 0);">
                <input type="hidden" name="post_type[]" value="post">
                <input type="hidden" name="post_type[]" value="page">
                <i class="fa fa-search"></i>
            </form>
        </div>
    </div>
</div>

<!-- main slider -->
<div class="container-fluid no-padding">
    <div class="tournament-header">
        <div class="header-background-img"></div>
        <div class="dots"></div>
        <div class="container">
            <div class="tournament-desc">
                <div class="tournament-desc-left">
                    <h1>
                        <img src="data:image/webp;base64,UklGRuIFAABXRUJQVlA4INYFAACwFwCdASo1AEkAPm0qjkWkIiEZm1Z8QAbEsQBZ2eiDzklW7sfBvPtmQ9uFOeKbtneX5DPeMits61CO4fFnvR+Hqexnzv3GpxKJZQNADxV8/T1luzJh//cyS/UNC3DK3IZoZbW/50qanun4DNcOOkNFRv3gt0BFTjb5oYdKrQ1A1nU8u5X5bL24IAX61SFe87k5lo1hnIq/MOEy8QpjUBmQskPFsifNoFbPIMiAbAIzr9GbO/HpMxWLKw2O69foh01cnPfYygAA/va3zKOspMaMjUY5CWjFj0N0mvJpfMVg06xBXpeoOGUV4JJgfoSlsAOBGPfZeYp05Vw6qaIQYqlN3dhwuuL30U+Kx9NIcBjhAIYtyTMcgEVcO2hK1yOFE8+LV329vCgqLV0N5woHr2vrkCdM4/Gny+I1UDUT5+UFsI3sW3KSFg2bBXoyr0WW3Pl6wckX4hWy5Wc/uzsvufOjCUaEHPwgLrmLQLLgp0M0IqL86XCXJ0MBZDUlP7b4K/Q4inRVqzJTSrEEQNgRk8y1QYKK4L1x+ju1rqpbsqwgYYaf8SVnebVYRYXRxR86f8eZVbhyDNU7YbyM7SCo+8vvSXKaOOlJg2lcNzqGzoGeup5b+PuscpjT8758HxzAU8oRY+NckjbJVHaYaM8fcNgeUxWmwlZculFyOE4THh7ALFy2MwnGymVBrVqhGGxZDfsLsnB+k6u28Su/awcfZRB8x3rFMwxTjcz6CRGisKklBMRkW+Nq2zfDiFwJRgSLCbPa3gIo7AwREbPsEVasENA6EyMVRotRqZejEAdgGimMCXVmXj6bWxxvaYJAnW92wFHPoE0FQKZSaUtXQI2xBUgxX1w9eDFxiR05pzdA40Vd4WggiIfz6ncoUPkfTGdJBGdtb46kVHRiuLQm7LS4VN/bRPUXNkZeNuPPZAaIKO/+6sTpRpEbWSDXuz2VciGnTc/JJE1sj+Gsekd8dyMmpc9Ah7m6HA+Xl4uHO3aphNW8D+rZJXjiSB+zbU5/maKQQ5NzPNAH7ZJMdcw2udokJ6eLhnKzpGKAaR7LptrSumMiPAnRgAWEoLZLyx1WThavZejtiIHlLl8OA/3hbraqIcDtm8zP9ARVHr5YUBOUZN8uGJXfm0zGn2ouHAksd2OIdcqIr2v3M734E85XILier6NfcrWQCH7tXKNphZBY7O/ZZsYhDfJ4EwfjSuHDptkKcbClj2SheZ6CaBeGkuyp8mstzhefO5fgs1OWKm+nknhU/fShYs9EV/Uo4oC7lv1flQLMSBfMrfcTMD9cj/01ZznmkzmdX4hEvNmqzOFRrmcGHf5HeEfkWussNaWzkY7o7W+RQjNKKpuZ6OdxsWLnjbO+T7JrpGYMemBHMiYEVC5LCxOIrBHhMHps5zPp8G6DrSu8vZQnc588NvTAvIS+TK7gK43lFM4m7e8l1jdBHI+YOYVM1PaRVb1RqTZ/ISU+nE8iwaEPqLy+GX0WuJMg7DkCjGFjSTtOm6mdKGfFNU60N2Rm+U7bqY+JOy/sRw5am13IvJ+CeHdgt7KLCBWWJtdo6qyv4IGbMRdvVi5oQDFFkhUWST07PqxQ+iHUHXBkvVaa8uYDXy2LOmUrOstIssNfk11XyBTveE3MwUHiITwf9UhnM2h3s1G3y6r0oQID5zxqyV7T3iKNWPBOoQ+W78il8Mpo+bnMMPA7gbcgF054f1PdKs9F/YNYbECVmH/7vzdpCKIC1UTjfwlePSxF+XwuFes1I0KgY/Cg8nRt5yx4j+sQYrA+a4p81w5d+iPEhW0QlGuS991vdQF/hCO4fv4TTSdBp0KJY7OUh08oCWjm1sx2NheSZN66jsu3RKeGGoW3r/phjk2MCrfFbbXcg6/Yk9fDSZ9MxZ239HT3qaDUInNsHUvADhhCU8kfPI+NJbEKOvWZM3j4eGtzXS8U2AuEodmkKGnsk7PsTDgD9FAKGjQjEARYADGBtvr8kkip3qAolrwAAAA=" alt="smite">
                        {{$data['TNAME']}}
                    </h1>
                    <div class="tournament-registration">
                        <!-- <p>Tournament starts in:</p>
                        <span id="tournament-count">{{$data['STARTDATE']}}</span> -->
                    </div>
                    <a href="#" class="button-small"><i class="fa fa-trophy"></i>Join Now</a>
                    <div class="clearfix"></div>
                </div>
                <div class="tournament-desc-right">
                    <p>{{$data['DESCRIPTION']}}</p>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="tournament-info">
                <div class="tournament-info-left">
                    <h2 id="tournament-date">
                        <strong>Starts</strong>
                        {{$data['STARTDATE']}}
                    </h2>
                    <!-- social networks share buttons -->
                    <div class="container share-social-wrapper">
                        <div class="share-social">
                            <a href="index.html" role="button" class="share-button"><i class="fa fa-plus"></i></a>
                            <a href="index.html" role="button" class="share-button"><i class="fa fa-reddit-alien"></i></a>
                            <a href="index.html" role="button" class="share-button"><i class="fa fa-facebook"></i></a>
                            <a href="index.html" role="button" class="share-button"><i class="fa fa-twitter"></i></a>
                            <a href="index.html" role="button" class="share-button"><i class="fa fa-link"></i></a>
                            <a href="index.html" role="button" class="share-button"><i class="fa fa-envelope"></i></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="tournament-info-right">
                    <h2>About</h2>
                    <ul>
                        <li class="participants">
                            <img src="{{asset('assets')}}/images/xteam.png.pagespeed.ic.zvEYobimQG.png" alt="team">
                            <h3>2 out of 32</h3>
                            <span>participants</span>
                        </li>
                        <li class="type">
                            <img src="{{asset('assets')}}/images/xdiagram.png.pagespeed.ic.wL67h7OUlu.png" alt="diagram">
                            <h3>Round Robbin with knockout</h3>
                            <span>Tournament type</span>
                        </li>
                        <li class="location">
                            <img src="{{asset('assets')}}/images/xlocation.png.pagespeed.ic.Ral-wh9QSp.png" alt="location">
                            <h3>{{$data['LOCATION']}}</h3>
                            <span>Location</span>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div class="top-divider"></div>

<!-- page content wrapper -->
<section class="content-wrapper container-fluid no-padding">
    <div class="container main-wrapper">

        <div class="accordion" id="accordion2">
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a href="#collapseOne" class="accordion-toggle show-more" data-toggle="collapse" data-parent="#accordion2">
                        <span id="show-more">HIDE TOURNAMENT DETAILS</span><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>
                    </a>
                </div>
                <div id="collapseOne" class="accordion-body">
                    <div class="tournament-details">
                        <h2>Tournament details</h2>
                        <ul>
                            <li>
                                <h3>Game</h3>
                                <span>Smite</span>
                            </li>
                            <li>
                                <h3>Contestants</h3>
                                <span>Teams</span>
                            </li>
                            <li>
                                <h3>Game format</h3>
                                <span>Best of 1</span>
                            </li>
                            <li>
                                <h3>Game frequency</h3>
                                <span>Daily</span>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="tournament-competitors">
                        <h2>Competitors </h2>
                        <ul class="teams-list">
                            <li class="single-team">
                                <div class="team-avatar"><a href="single-team.html"><img src="{{asset('assets')}}/images/xcloud9-counterstrike-wallpaper-50x50.jpg.pagespeed.ic.7Grqv_Wqdi.webp"></a>
                                </div>
                                <div class="team-info">
                                    <a href="single-team.html" class="team-title">cloud 9</a>
                                    <span class="members">6 members</span>
                                </div>
                            </li>

                            <li class="single-team">
                                <div class="team-avatar"><a href="single-team.html"><img src="{{asset('assets')}}/images/xAvatar_Large-50x50.png.pagespeed.ic.JS4ZZ8R0QW.webp"></a></div>
                                <div class="team-info">
                                    <a href="single-team.html" class="team-title"> Team gg</a>
                                    <span class="members">6 members</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="tournament-regulation news-tabbed">
                        <h2>Regulations</h2>
                        <ul class="nav nav-tabs" role="tablist" id="regulations-tabs">
                            <li role="presentation" class="active">
                                <a href="#regulations_tab_0" aria-controls="regulations_tab_0" role="tab" data-toggle="tab">disputes</a>
                            </li>
                            <li role="presentation"><a href="#regulations_tab_1" aria-controls="regulations_tab_1" role="tab" data-toggle="tab">prizes</a></li>
                            <li role="presentation"><a href="#regulations_tab_2" aria-controls="regulations_tab_2" role="tab" data-toggle="tab">registration rules</a></li>
                            <li role="presentation"><a href="#regulations_tab_3" aria-controls="regulations_tab_3" role="tab" data-toggle="tab">victory conditions</a></li>
                            <li role="presentation"><a href="#regulations_tab_4" aria-controls="regulations_tab_4" role="tab" data-toggle="tab">useful links</a></li>
                        </ul>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="regulations_tab_0">
                                <div class="regulation-text">
                                    <h3>DISPUTES</h3>
                                    <h3><b>MORBI HENDRERIT</b><b>VESTIBULUM ANTE IPSUM PRIMIS IN FAUCIBUS ORCI LUCTUS
                                            E</b><b>&nbsp;EST UT VELIT LOBORTIS, ULLAMCORPER COMMODO.</b></h3>
                                    SED TRISTIQUE, MAGNA FAUCIBUS ELEMENTUM VARIUS, MAGNA NEQUE VENENATIS TELLUS, EGET
                                    ALIQUAM NUNC ELITNUNC GRAVIDA VELIT:
                                    <b><br></b>
                                    <ul>
                                        <li>Sed est ante, ornare vel vehicula non, pretium et arcu. Nunc justo ipsum,
                                            gravida at congue at, consectetur non leo. Mauris sit amet ultrices
                                            ante.<br></li>
                                        <li>Cras ut pretium erat, eget maximus augue.<br></li>
                                        <li>Donec aliquam dolor eget arcu finibus, et rhoncus orci rutrum.<br></li>
                                        <li>Pellentesque volutpat justo ligula, eget lobortis augue ultrices eget.<br>
                                        </li>
                                    </ul>
                                    QUISQUE VESTIBULUM VELIT MASSA, ID VIVERRA MAURIS PULVINAR AC. SED TRISTIQUE, MAGNA
                                    FAUCIBUS ELEMENTUM VARIUS:
                                    <ul>
                                        <li>Quisque vestibulum pharetra risus, ac ultrices neque pretium id.<br></li>
                                        <li>Integer id elementum magna.&nbsp;<br></li>
                                        <li>Interdum et malesuada fames ac ante ipsum primis in faucibus.&nbsp;<br></li>
                                        <li>Aliquam et imperdiet orci, in dapibus diam. Praesent felis massa,
                                            consectetur in mi non, bibendum vehicula ligula.<br></li>
                                        <li>Maecenas finibus ligula nec sapien dignissim convallis. Nulla posuere, nisi
                                            at blandit mattis, eros leo sodales mauris, et placerat sapien tellus sed
                                            odio.<br></li>
                                        <li>Donec luctus condimentum magna a pulvinar.</li>
                                    </ul>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="regulations_tab_1">
                                <div class="regulation-text">
                                    <h3>PRIZES</h3>
                                    <ul>
                                        <li>Quisque vestibulum pharetra risus, ac ultrices neque pretium id.<br></li>
                                        <li>Integer id elementum magna.&nbsp;<br></li>
                                        <li>Interdum et malesuada fames ac ante ipsum primis in faucibus.&nbsp;<br></li>
                                        <li>Aliquam et imperdiet orci, in dapibus diam. Praesent felis massa,
                                            consectetur in mi non, bibendum vehicula ligula.</li>
                                    </ul>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="regulations_tab_2">
                                <div class="regulation-text">
                                    <h3>REGISTRATION RULES</h3>
                                    <ul>
                                        <li>Sed est ante, ornare vel vehicula non, pretium et arcu. Nunc justo ipsum,
                                            gravida at congue at, consectetur non leo. Mauris sit amet ultrices ante.
                                        </li>
                                        <li>Quisque vestibulum pharetra risus, ac ultrices neque pretium id.<br></li>
                                        <li>Integer id elementum magna.&nbsp;<br></li>
                                        <li>Interdum et malesuada fames ac ante ipsum primis in faucibus.&nbsp;<br></li>
                                        <li>Aliquam et imperdiet orci, in dapibus diam. Praesent felis massa,
                                            consectetur in mi non, bibendum vehicula ligula.</li>
                                    </ul>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="regulations_tab_3">
                                <div class="regulation-text">
                                    <h3>VICTORY CONDITIONS</h3>
                                    <h3>GROUP STAGE</h3>
                                    <ul>
                                        <li>Sed est ante, ornare vel vehicula non, pretium et arcu. Nunc justo ipsum,
                                            gravida at congue at, consectetur non leo. Mauris sit amet ultrices
                                            ante.<br></li>
                                        <li>Cras ut pretium erat, eget maximus augue.<br></li>
                                        <li>Donec aliquam dolor eget arcu finibus, et rhoncus orci rutrum.</li>
                                        <li>Interdum et malesuada fames ac ante ipsum primis in faucibus.&nbsp;<br></li>
                                        <li>Aliquam et imperdiet orci, in dapibus diam. Praesent felis massa,
                                            consectetur in mi non, bibendum vehicula ligula.</li>
                                    </ul>
                                    <h3>DRAW RULES</h3>
                                    MORBI HENDRERIT EST UT VELIT LOBORTIS, ULLAMCORPER COMMODO IPSUM CONSEQUAT.
                                    VESTIBULUM ANTE IPSUM PRIMIS
                                    <br>
                                    <br>
                                    Sed est ante, ornare vel vehicula non, pretium et arcu. Nunc justo ipsum, gravida at
                                    congue at, consectetur non leo. Mauris sit amet ultrices ante. Cras ut pretium erat,
                                    eget maximus augue. Donec aliquam dolor eget arcu finibus, et rhoncus orci rutrum.
                                    Pellentesque volutpat justo ligula, eget lobortis augue ultrices eget. .&nbsp;
                                    <br>
                                    <br>
                                    MAGNA NEQUE VENENATIS TELLUS, EGET ALIQUAM NUNC ELIT NEC MAGNA. NUNC GRAVIDA VELIT A
                                    QUAM PHARETRA, SED ULTRICIES TURPIS VESTIBULUM
                                    <br>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="regulations_tab_4">
                                <div class="regulation-text">
                                    <a href="blog-style.html">
                                        <h3>USEFUL LINKS</h3>
                                        <ul>
                                            <li><a target="_blank" rel="nofollow">Morbi hendrerit est&nbsp;</a></li>
                                            <li><a target="_blank" rel="nofollow">Ut velit lobortis</a></li>
                                            <li><a target="_blank" rel="nofollow">Ullamcorper commodo&nbsp;</a></li>
                                            <li><a target="_blank" rel="nofollow">Opsum consequat</a></li>
                                            <li><a target="_blank" rel="nofollow">Vestibulum ante ipsum primis</a></li>
                                            <li><a target="_blank" rel="nofollow">In faucibus orci luctus&nbsp;</a></li>
                                            <li><a target="_blank" rel="nofollow">Et ultrices posuere cubilia Curae</a>
                                            </li>
                                        </ul>
                                    </a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="tournament-prize">
            <h2>Winner Prizes</h2>
            <table cellpadding="0" cellspacing="0" id="prizes-table">
                <tbody>
                    <tr>
                        <th><img src="{{asset('assets')}}/images/xlaurel.png.pagespeed.ic.0G9N0uKNkP.png"><span>Place</span></th>
                        <th><img src="{{asset('assets')}}/images/xcup.png.pagespeed.ic.RQFm6jJx2k.png"><span>Reward</span></th>
                    </tr>
                    <tr>
                        <td class="firstrow"><span><img src="{{asset('assets')}}/images/x1st.png.pagespeed.ic.Jypez9Qvap.png"> place
                                1st</span></td>
                        <td class="trcell"><span class="tournament-prize">$10000</span></td>
                    </tr>
                    <tr>
                        <td class="secondrow"><span><img src="{{asset('assets')}}/images/x2nd.png.pagespeed.ic.P5zsPuQc7t.png"> place
                                2nd</span></td>
                        <td class="trcell"><span class="tournament-prize">$8500</span></td>
                    </tr>
                    <tr>
                        <td class="thirdrow"><span><img src="{{asset('assets')}}/images/x3rd.png.pagespeed.ic.HYDTLy4H2t.png"> place
                                3rd</span></td>
                        <td class="trcell"><span class="tournament-prize">$2500</span></td>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>
</section>

@endsection

@push('scripts')

@endpush