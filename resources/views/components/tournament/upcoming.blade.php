@extends('components.master')
{{-- Meta Tag HTML --}}
@section('link-cannonical', url()->current())
@section('meta-fb-url', url()->current())
@section('body_class', 'blog-style teams-page')

{{-- END META TAG --}}
@section('content')
<section class="content-wrapper container-fluid no-padding">
    <section class="tournaments container">
        <!-- content title -->
        <div class="col-lg-12 main-content-title">
            <h3><i class="fa fa-gamepad"></i> tournaments</h3>
        </div>
        @foreach($data as $tour_list)
        <!-- single tournament -->
        <div class="col-lg-6 single-tournament">
            <a href="{{url('tournament-detail/'.$tour_list['ID'])}}">
                <h4>{{$tour_list['TNAME']}}</h4>
            </a>
            <div class="tournament-image">
                <a href="{{url('tournament-detail/'.$tour_list['ID'])}}"><img src="{{asset('assets')}}/images/cs.jpg"></a>
            </div>
            <div class="tournament-text">
                <p><span>Tournament Begins:</span>{{$tour_list['STARTDATE']}}</p>
                <br>
                <p><span>Maximum participants:</span> {{$tour_list['TQUOTA']}} teams</p>
                <!-- <p><span>Current participants:</span> 4 teams</p> -->
                <br>
                <!-- <p class="tournament-prize"><span>Prize:</span> $5000, $2500, $1000, CS:GO T-Shirt</p> -->
            </div>
        </div>
        @endforeach
    </section>
</section>
@endsection

@push('scripts')

@endpush