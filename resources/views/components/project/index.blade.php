@extends('components.master')
{{-- Meta Tag HTML --}}
@section('link-cannonical', url()->current())
@section('meta-desc', '')
@section('meta-fb-title', '')
@section('meta-fb-desc', '')
@section('meta-fb-image', '')
@section('meta-fb-url', url()->current())
{{-- END META TAG --}}
@section('content')
<section class="section-project-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                <div class="filter">
                    <div class="title-widget-filter">
                        Category
                    </div>
                    <div class="filter-content">
                        <button class="active" data-type="*" name="isotope-filter">
                            All
                        </button>
                        @foreach($category as $item)
                        <button class="" data-type="{{$item['ID']}}" name="{{$item['ID']}}">
                            {{$item['CATEGORY']}}
                        </button>
                        @endforeach
                    </div>
                </div>
                <div class="filter">
                    <div class="title-widget-filter">
                        Type Of Client
                    </div>
                    <div class="filter-content">
                        <ul>
                            <li><a href="javascript:void(0)" class="active">Retail</a></li>
                            <li><a href="javascript:void(0)">Arts</a></li>
                            <li><a href="javascript:void(0)">Print</a></li>
                            <li><a href="javascript:void(0)">Services</a></li>
                            <li><a href="javascript:void(0)">Institusi</a></li>
                            <li><a href="javascript:void(0)">Media</a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="project-grid">
                    @forelse($project as $item)
                    <div class="project-item" data-type="{{$item['CATEGORY']}}">
                        <figure class="figure">
                            <img src="{{$item['IMAGE']}}" class="figure-img img-fluid" alt="title">
                            <figcaption class="figure-caption">{{$item['TITLE']}}</figcaption>
                            <div class="figure-status">Client: <span class="figure-content">PT. Zurich Insurance
                                    Indonesia</span></div>
                            <div class="figure-status">Work: <span class="figure-content">Event Management</span></div>
                        </figure>
                    </div>
                    @empty
                    <h3>data Kosong</h3>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</section>




@endsection

@push('scripts')
<script src="https://unpkg.com/isotope-layout@3.0.6/dist/isotope.pkgd.min.js"></script>
<script>
// external js: isotope.pkgd.js

// init Isotope elements
var $box = $(".project-grid").isotope({
    itemSelector: ".project-item"
});
// filter functions
// bind filter button click
$(".filter-content").on("click", "button", function() {
    var filterValue = $(this).attr("data-type");
    $(".filter-content").find(".active").removeClass("active");
    $(this).addClass("active");
    if (filterValue !== "*") {
        filterValue = '[data-type="' + filterValue + '"]';
    }
    console.log(filterValue);
    $box.isotope({
        filter: filterValue
    });
});

// change is-checked class on buttons
</script>
@endpush