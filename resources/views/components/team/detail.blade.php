@extends('components.master')
@section('body_class', 'single-match-page single-team-page')
@section('content')

@include('components.include.news-ticker')

<!-- main slider -->
<div class="container-fluid main-slider no-padding">
    <div class="slider">
        <div class="container">
            <div class="team-a team-b">
                <div class="team-title">
                    <h1>{{$team['TEAM_NAME']}}</h1>
                </div>
                <div class="team-img">
                    <a href="profile.html"><img src="{{$team['LOGO']}}"></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="top-divider"></div>

<!-- page content wrapper -->
<section class="content-wrapper container-fluid no-padding">

    <div class="container-fluid no-padding">
        <ul class="nav nav-tabs team-nav">
            <li class="active">
                <a href="#team" data-toggle="tab">
                    <i class="fa fa-flag"></i>&nbsp;Team Page
                </a>
            </li>
            <li>
                <a data-toggle="tab" href="#members_tab">
                    <i class="fa fa-users"></i> Members
                </a>
            </li>
            <li>
                <a data-toggle="tab" href="#matches-tab">
                    <i class="fa fa-crosshairs"></i> Matches
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active container" id="team">
                <div class="main-content col-lg-8">
                    <!-- content title -->
                    <div class="main-content-title">
                        <h3>about us</h3>
                    </div>
                    <div class="blog-content">
                        <p>{{$team['DESCRIPTION']}}</p>
                    </div>

                    <!-- content title -->
                    <div class="main-content-title">
                        <h3><i class="fa fa-newspaper-o"></i> team discussion</h3>
                    </div>

                    <form id="commentform">

                        <div class="form-section">
                            <input id="author" name="author" placeholder="Name*" type="text" value="" size="30" maxlength="20" tabindex="3">
                        </div>
                        <div class="form-section">
                            <input placeholder="Email*" id="email" name="email" type="text" value="" size="30" maxlength="50" tabindex="4">
                        </div>
                        <div class="form-section">
                            <input id="url" placeholder="Website" name="url" type="text" value="" size="30" maxlength="50" tabindex="5">
                        </div>
                        <div class="form-section">
                            <textarea placeholder="Leave a comment..." id="comment" name="comment" cols="45" rows="8" tabindex="6"></textarea>
                        </div>
                        <div class="form-submit">
                            <input id="submit" name="submit" class="button-small button-green" type="submit" value="Submit comment" tabindex="7">
                        </div>
                    </form>
                </div>
                <div class="sidebar col-lg-4">

                    <div class="team-info-wrapper">
                        <div class="team-info-members">
                            <i class="fa fa-users"></i>
                            <strong> 4&nbsp;Members</strong>
                        </div>
                        <ul class="team-info-location">
                            <li><strong>Language</strong>English</li>
                            <li><strong>Location</strong>United Kingdom</li>
                        </ul>
                        <ul class="team-info-links">
                            <li><a href="http://skywarriorthemes.com">Official site<i class="fa fa-external-link"></i></a></li>
                            <li><a href="http://skywarriorthemes.com">Buy us a coffee!<i class="fa fa-external-link"></i></a></li>
                        </ul>
                    </div>

                    <!-- content title -->
                    <div class="main-content-title">
                        <h3><i class="fa fa-gamepad"></i> GAMES</h3>
                    </div>

                    <!-- review post list -->
                    <ul class="sidebar-review">

                        <!-- single review -->
                        <li>
                            <a href="{{url('/match-detail')}}" style="background-image: url({{asset('assets')}}/images/csb-1168x230.jpg);">
                                <img alt="img" src="{{asset('assets')}}/images/cs-85x116.jpg">
                                <strong>Counter Strike: Global Offensive </strong>
                            </a>
                        </li>

                        <!-- single review -->
                        <li>
                            <a href="{{url('/match-detail')}}" style="background-image: url({{asset('assets')}}/images/mercy_by_numyumy-da5e4f2-1168x230.jpg);">
                                <img alt="img" src="{{asset('assets')}}/images/ow-85x116.jpg">
                                <strong>Overwatch </strong>
                            </a>
                        </li>

                        <!-- single review -->
                        <li>
                            <a href="{{url('/match-detail')}}" style="background-image: url({{asset('assets')}}/images/smiteb-1168x230.jpg);">
                                <img alt="img" src="{{asset('assets')}}/images/smite-85x116.jpg">
                                <strong>Smite </strong>
                            </a>
                        </li>

                    </ul>

                    <!-- content title -->
                    <div class="main-content-title">
                        <h3><i class="fa fa-crosshairs"></i> latest matches </h3>
                    </div>

                    <!-- matches -->
                    <div class="tab-content-wrapper">
                        <ul class="nav nav-tabs matches-tab-wrapper">
                            <li><a data-toggle="tab" href="#h">ALL</a></li>
                            <li class="active"><a data-toggle="tab" href="#m1">CS:GO</a></li>
                            <li><a data-toggle="tab" href="#m2">LOL</a></li>
                        </ul>

                        <div class="tab-content">
                            <div id="h" class="tab-pane fade">

                                <!-- matches list -->
                                <ul class="match-list">

                                    <!-- single match -->
                                    <li class="single-match-wrapper">
                                        <a href="{{url('/match-detail')}}">
                                            <span class="result win">23:20</span>
                                            <div class="single-match">
                                                <img src="{{asset('assets')}}/images/qN1JFnkg-25x25.png">
                                                <span class="vs">vs</span>
                                                <img src="{{asset('assets')}}/images/cloud9-counterstrike-wallpaper-25x25.jpg">
                                            </div>
                                            <span class="date">CS:GO - October 4, 2016, 1:58 pm</span>
                                        </a>
                                    </li>

                                    <!-- single match -->
                                    <li class="single-match-wrapper">
                                        <a href="{{url('/match-detail')}}">
                                            <span class="result lose">12:16</span>
                                            <div class="single-match">
                                                <img src="{{asset('assets')}}/images/logo-helmet-25x25.jpg">
                                                <span class="vs">vs</span>
                                                <img src="{{asset('assets')}}/images/as-25x25.jpg">
                                            </div>
                                            <span class="date">CS:GO - May 5, 2016, 9:00 pm</span>
                                        </a>
                                    </li>

                                    <!-- single match -->
                                    <li class="single-match-wrapper">
                                        <a href="{{url('/match-detail')}}">
                                            <span class="result">0:0</span>
                                            <div class="single-match">
                                                <img src="{{asset('assets')}}/images/13-1-25x25.jpg">
                                                <span class="vs">vs</span>
                                                <img src="{{asset('assets')}}/images/20-1-25x25.jpg">
                                            </div>
                                            <span class="date">CS:GO - May 12, 2016, 9:00 pm</span>
                                        </a>
                                    </li>

                                    <!-- single match -->
                                    <li class="single-match-wrapper">
                                        <a href="{{url('/match-detail')}}">
                                            <span class="result win">43:34</span>
                                            <div class="single-match">
                                                <img src="{{asset('assets')}}/images/16-25x25.jpg">
                                                <span class="vs">vs</span>
                                                <img src="{{asset('assets')}}/images/19-1-25x25.jpg">
                                            </div>
                                            <span class="date">CS:GO - October 4, 2016, 1:58 pm</span>
                                        </a>
                                    </li>

                                    <!-- single match -->
                                    <li class="single-match-wrapper">
                                        <a href="{{url('/match-detail')}}">
                                            <span class="result win">24:22</span>
                                            <div class="single-match">
                                                <img src="{{asset('assets')}}/images/qN1JFnkg-25x25.png">
                                                <span class="vs">vs</span>
                                                <img src="{{asset('assets')}}/images/logo-helmet-25x25.jpg">
                                            </div>
                                            <span class="date">LoL - April 8, 2016, 8:10 pm</span>
                                        </a>
                                    </li>

                                    <!-- single match -->
                                    <li class="single-match-wrapper">
                                        <a href="{{url('/match-detail')}}">
                                            <span class="result win">33:14</span>
                                            <div class="single-match">
                                                <img src="{{asset('assets')}}/images/as-25x25.jpg">
                                                <span class="vs">vs</span>
                                                <img src="{{asset('assets')}}/images/cloud9-counterstrike-wallpaper-25x25.jpg">
                                            </div>
                                            <span class="date">LoL - April 8, 2016, 8:10 pm</span>
                                        </a>
                                    </li>

                                    <!-- single match -->
                                    <li class="single-match-wrapper">
                                        <a href="{{url('/match-detail')}}">
                                            <span class="result lose">12:16</span>
                                            <div class="single-match">
                                                <img src="{{asset('assets')}}/images/20-1-25x25.jpg">
                                                <span class="vs">vs</span>
                                                <img src="{{asset('assets')}}/images/13-1-25x25.jpg">
                                            </div>
                                            <span class="date">LoL - April 8, 2016, 8:10 pm</span>
                                        </a>
                                    </li>

                                    <!-- single match -->
                                    <li class="single-match-wrapper">
                                        <a href="{{url('/match-detail')}}">
                                            <span class="result lose">25:27</span>
                                            <div class="single-match">
                                                <img src="{{asset('assets')}}/images/19-1-25x25.jpg">
                                                <span class="vs">vs</span>
                                                <img src="{{asset('assets')}}/images/16-25x25.jpg">
                                            </div>
                                            <span class="date">LoL - April 8, 2016, 8:10 pm</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div id="m1" class="tab-pane fade in active">
                                <!-- matches list -->
                                <ul class="match-list">

                                    <!-- single match -->
                                    <li class="single-match-wrapper">
                                        <a href="{{url('/match-detail')}}">
                                            <span class="result lose">12:16</span>
                                            <div class="single-match">
                                                <img src="{{asset('assets')}}/images/logo-helmet-25x25.jpg">
                                                <span class="vs">vs</span>
                                                <img src="{{asset('assets')}}/images/as-25x25.jpg">
                                            </div>
                                            <span class="date">CS:GO - May 5, 2016, 9:00 pm</span>
                                        </a>
                                    </li>

                                    <!-- single match -->
                                    <li class="single-match-wrapper">
                                        <a href="{{url('/match-detail')}}">
                                            <span class="result win">43:34</span>
                                            <div class="single-match">
                                                <img src="{{asset('assets')}}/images/16-25x25.jpg">
                                                <span class="vs">vs</span>
                                                <img src="{{asset('assets')}}/images/19-1-25x25.jpg">
                                            </div>
                                            <span class="date">CS:GO - October 4, 2016, 1:58 pm</span>
                                        </a>
                                    </li>

                                    <!-- single match -->
                                    <li class="single-match-wrapper">
                                        <a href="{{url('/match-detail')}}">
                                            <span class="result win">33:14</span>
                                            <div class="single-match">
                                                <img src="{{asset('assets')}}/images/as-25x25.jpg">
                                                <span class="vs">vs</span>
                                                <img src="{{asset('assets')}}/images/cloud9-counterstrike-wallpaper-25x25.jpg">
                                            </div>
                                            <span class="date">LoL - April 8, 2016, 8:10 pm</span>
                                        </a>
                                    </li>

                                    <!-- single match -->
                                    <li class="single-match-wrapper">
                                        <a href="{{url('/match-detail')}}">
                                            <span class="result lose">25:27</span>
                                            <div class="single-match">
                                                <img src="{{asset('assets')}}/images/19-1-25x25.jpg">
                                                <span class="vs">vs</span>
                                                <img src="{{asset('assets')}}/images/16-25x25.jpg">
                                            </div>
                                            <span class="date">LoL - April 8, 2016, 8:10 pm</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div id="m2" class="tab-pane fade">

                                <!-- matches list -->
                                <ul class="match-list">
                                    <!-- single match -->
                                    <li class="single-match-wrapper">
                                        <a href="{{url('/match-detail')}}">
                                            <span class="result win">23:20</span>
                                            <div class="single-match">
                                                <img src="{{asset('assets')}}/images/qN1JFnkg-25x25.png">
                                                <span class="vs">vs</span>
                                                <img src="{{asset('assets')}}/images/cloud9-counterstrike-wallpaper-25x25.jpg">
                                            </div>
                                            <span class="date">CS:GO - October 4, 2016, 1:58 pm</span>
                                        </a>
                                    </li>

                                    <!-- single match -->
                                    <li class="single-match-wrapper">
                                        <a href="{{url('/match-detail')}}">
                                            <span class="result">0:0</span>
                                            <div class="single-match">
                                                <img src="{{asset('assets')}}/images/13-1-25x25.jpg">
                                                <span class="vs">vs</span>
                                                <img src="{{asset('assets')}}/images/20-1-25x25.jpg">
                                            </div>
                                            <span class="date">CS:GO - May 12, 2016, 9:00 pm</span>
                                        </a>
                                    </li>

                                    <!-- single match -->
                                    <li class="single-match-wrapper">
                                        <a href="{{url('/match-detail')}}">
                                            <span class="result win">24:22</span>
                                            <div class="single-match">
                                                <img src="{{asset('assets')}}/images/qN1JFnkg-25x25.png">
                                                <span class="vs">vs</span>
                                                <img src="{{asset('assets')}}/images/logo-helmet-25x25.jpg">
                                            </div>
                                            <span class="date">LoL - April 8, 2016, 8:10 pm</span>
                                        </a>
                                    </li>

                                    <!-- single match -->
                                    <li class="single-match-wrapper">
                                        <a href="{{url('/match-detail')}}">
                                            <span class="result lose">12:16</span>
                                            <div class="single-match">
                                                <img src="{{asset('assets')}}/images/20-1-25x25.jpg">
                                                <span class="vs">vs</span>
                                                <img src="{{asset('assets')}}/images/13-1-25x25.jpg">
                                            </div>
                                            <span class="date">LoL - April 8, 2016, 8:10 pm</span>
                                        </a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="tab-pane container" id="members_tab">
                <div class="col-lg-12 no-padding">
                    <ul class="teams-list">
                        @foreach($member as $team_member)
                        <li class="single-team">
                            <div class="team-avatar"><a href="profile.html"><img src="{{asset('assets')}}/images/18-1.jpg"></a></div>
                            <div class="team-info">
                                <a href="profile.html" class="team-title">{{$team_member['USERNAME']}}</a>
                                <!-- <span class="members">Joined: Aug, 2016</span> -->
                            </div>
                            <div class="is-admin">
                                @if($team_member['USERNAME'] == $team['LEADER'])
                                <i class="fa-star fa"></i>
                                @endif
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="tab-pane" id="matches-tab">
                <div class="col-lg-12 no-padding">
                    <!-- main slider -->
                    <div class="container-fluid no-padding">
                        <div class="slider">
                            <div class="dots"></div>
                            <div class="container">
                                <div class="team-a">
                                    <a href="single-team-page.html" class="team-title"><span>Natus Vincere</span></a>
                                    <div class="team-img">
                                        <a href="single-team-page.html"><img src="{{asset('assets')}}/images/as-210x178.jpg"></a>
                                        <div class="team-result"><span>18</span></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <span class="versus">vs</span>
                                <div class="team-a team-b">
                                    <a href="single-team-page.html" class="team-title"><span>team gg</span></a>
                                    <div class="team-img">
                                        <a href="single-team-page.html"><img src="{{asset('assets')}}/images/Avatar_Large-210x178.png"></a>
                                        <div class="team-result"><span>12</span></div>
                                    </div>
                                </div>
                                <div class="tournament-meta">
                                    <div class="tournament-info">
                                        <strong>ESL Open Cup</strong>
                                        <i class="fa fa-calendar"></i>
                                        <span>November 17, 2015 10:00 pm</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="top-divider"></div>

                        <div class="container">
                            <ul class="tab-matches-list">
                                <li class="lost">
                                    <a href="{{url('/match-detail')}}">
                                        <div class="tab-team-a">
                                            <img src="{{asset('assets')}}/images/lK1NsvDK-210x178.png">
                                            <span>13</span>
                                        </div>
                                        <strong>VS</strong>
                                        <div class="tab-team-b">
                                            <img src="{{asset('assets')}}/images/Avatar_Large-210x178.png">
                                            <span>17</span>
                                        </div>
                                        <div class="tab-match-info">
                                            <strong>MOBA weekend #2</strong>
                                            <i class="fa fa-calendar"></i>
                                            <span> June 12, 2015 6:30 pm</span>
                                        </div>
                                        <div class="match-game">
                                            <img alt="img" src="{{asset('assets')}}/images/smite-85x116.jpg" class="mgame">
                                        </div>
                                        <div class="match-status"></div>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>

                                <li class="default">
                                    <a href="{{url('/match-detail')}}">
                                        <div class="tab-team-a">
                                            <img src="{{asset('assets')}}/images/lK1NsvDK-210x178.png">
                                            <span>0</span>
                                        </div>
                                        <strong>VS</strong>
                                        <div class="tab-team-b">
                                            <img src="{{asset('assets')}}/images/Avatar_Large-210x178.png">
                                            <span>0</span>
                                        </div>
                                        <div class="tab-match-info">
                                            <strong>Best of 5</strong>
                                            <i class="fa fa-calendar"></i>
                                            <span> July 29, 2015 3:30 pm</span>
                                        </div>
                                        <div class="match-game">
                                            <img alt="img" src="{{asset('assets')}}/images/smite-85x116.jpg" class="mgame">
                                        </div>
                                        <div class="match-status"></div>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>

                                <li class="won">
                                    <a href="{{url('/match-detail')}}">
                                        <div class="tab-team-a">
                                            <img src="{{asset('assets')}}/images/lK1NsvDK-210x178.png">
                                            <span>18</span>
                                        </div>
                                        <strong>VS</strong>
                                        <div class="tab-team-b">
                                            <img src="{{asset('assets')}}/images/Avatar_Large-210x178.png">
                                            <span>12</span>
                                        </div>
                                        <div class="tab-match-info">
                                            <strong>MOBA weekend #2</strong>
                                            <i class="fa fa-calendar"></i>
                                            <span> June 12, 2015 6:30 pm</span>
                                        </div>
                                        <div class="match-game">
                                            <img alt="img" src="{{asset('assets')}}/images/smite-85x116.jpg" class="mgame">
                                        </div>
                                        <div class="match-status"></div>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

</section>

@endsection