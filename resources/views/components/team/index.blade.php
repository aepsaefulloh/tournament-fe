@extends('components.master')
@section('body_class', 'blog-style teams-page')
@section('content')
<!-- main slider -->
<div class="container-fluid no-padding">
    <div class="slider col-lg-12">
        <h1>Teams</h1>
        <strong><a href="{{url('/')}}">Home</a> <span>/ Teams</span></strong>
    </div>
</div>

@include('components.include.news-ticker')

<!-- page content wrapper -->
<section class="content-wrapper container-fluid no-padding">
    <div class="container main-wrapper">
        <div style="display: flex;justify-content: space-between;align-items: center;">
            <form class="search-teams">
                <input type="text" autocomplete="off" name="team_name" id="team_name" placeholder="Search Teams...">
                <input type="button" id="members_search_submit" name="members_search_submit" value="Search" class="button-medium">
            </form>
            <button class="button-medium">Add New Team</button>
        </div>

        <ul class="teams-list">
            @foreach($team as $allTeams)
            <li class="single-team">
                <div class="team-avatar"><a href="{{url('/team-detail/'.$allTeams['ID'])}}"><img src="{{$allTeams['LOGO']}}"></a></div>
                <div class="team-info">
                    <a href="{{url('/team-detail/'.$allTeams['ID'])}}" class="team-title">{{$allTeams['TEAM_NAME']}}</a>
                    <span class="members">{{$allTeams['total_member']}} members</span>
                </div>
            </li>
            @endforeach
        </ul>

    </div>
</section>

@endsection