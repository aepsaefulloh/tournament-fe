@extends('components.master')
@section('body_class', 'blog-style matches-page')
@section('content')
<!-- main slider -->
<div class="container-fluid no-padding">
    <div class="slider col-lg-12">
        <h1>Matches</h1>
        <strong><a href="{{url('/')}}">Home</a> <span>/ Matches</span></strong>
    </div>
</div>

@include('components.include.news-ticker')

<!-- page content wrapper -->
<section class="content-wrapper container-fluid no-padding">
    <div class="container main-wrapper">

        <!-- matches -->
        <div class="tab-content-wrapper">
            <ul class="nav nav-tabs matches-tab-wrapper">
                <li class="active"><a data-toggle="tab" href="#g">ALL</a></li>
                <li><a data-toggle="tab" href="#g1">CS:GO</a></li>
                <li><a data-toggle="tab" href="#g2">LOL</a></li>
            </ul>

            <div class="tab-content">
                <div id="g" class="tab-pane fade in active">

                    <!-- matches list -->
                    <ul class="match-list">

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result">Upcoming</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/qN1JFnkg-50x50.png">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/Avatar_Large-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>WINNER TAKES IT ALL! #8</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result win">13:11</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/as-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/Avatar_Large-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>Dawn of the august</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result win">18:12</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/qN1JFnkg-50x50.png">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/a25c884d950a539b9a54e2f79206ebe06237-210x178-50x50.jpg">
                                </div>
                                <div class="date">
                                    <strong>ESL Open Cup</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result lose">16:17</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/qN1JFnkg-50x50.png">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/a25c884d950a539b9a54e2f79206ebe06237-210x178-50x50.jpg">
                                </div>
                                <div class="date">
                                    <strong>Skadowitche 16</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result">Upcoming</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/as-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/lK1NsvDK-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>Best of 5</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result lose">11:16</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/logo-helmet-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/a25c884d950a539b9a54e2f79206ebe06237-210x178-50x50.jpg">
                                </div>
                                <div class="date">
                                    <strong>Friday warmup</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result win">33:24</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/as-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/Avatar_Large-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>CoD night</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result lose">23:27</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/logo-helmet-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/a25c884d950a539b9a54e2f79206ebe06237-210x178-50x50.jpg">
                                </div>
                                <div class="date">
                                    <strong>SF World Championships</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result win">12:10</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/qN1JFnkg-50x50.png">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/cloud9-counterstrike-wallpaper-210x178-50x50.jpg">
                                </div>
                                <div class="date">
                                    <strong>MOBA weekend #2</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result">Upcoming</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/logo-helmet-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/lK1NsvDK-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>Brothers in arms - best of 3</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result win">17:15</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/a25c884d950a539b9a54e2f79206ebe06237-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/Avatar_Large-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>5 vs 5</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result win">21:20</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/as-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/lK1NsvDK-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>D-Day tournament - round 4</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result lose">10:19</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/qN1JFnkg-50x50.png">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/lK1NsvDK-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>GO4CS:GO Europe cup #112</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result">Upcoming</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/logo-helmet-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/lK1NsvDK-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>Best of 3</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result win">43:40</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/logo-helmet-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/a25c884d950a539b9a54e2f79206ebe06237-210x178-50x50.jpg">
                                </div>
                                <div class="date">
                                    <strong>Asia open 2016</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result win">35:28</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/as-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/Avatar_Large-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>ESL SUN</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result lose">14:15</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/as-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/cloud9-counterstrike-wallpaper-210x178-50x50.jpg">
                                </div>
                                <div class="date">
                                    <strong>D2 Open Sept</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result lose">0:25</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/qN1JFnkg-50x50.png">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/lK1NsvDK-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>Saturday fun</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result">Upcoming</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/logo-helmet-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/lK1NsvDK-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>Saturday fun</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result">Upcoming</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/as-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/a25c884d950a539b9a54e2f79206ebe06237-210x178-50x50.jpg">
                                </div>
                                <div class="date">
                                    <strong>Friendly</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                    </ul>
                </div>
                <div id="g1" class="tab-pane fade">
                    <!-- matches list -->
                    <ul class="match-list">

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result win">13:11</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/logo-helmet-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/lK1NsvDK-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>Friday warmup</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result lose">16:17</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/as-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/Avatar_Large-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>CoD night</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result lose">11:16</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/as-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/lK1NsvDK-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>Brothers in arms - best of 3</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result lose">23:27</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/qN1JFnkg-50x50.png">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/cloud9-counterstrike-wallpaper-210x178-50x50.jpg">
                                </div>
                                <div class="date">
                                    <strong>D-Day tournament - round 4</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result">Upcoming</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/logo-helmet-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/lK1NsvDK-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>ESL Open Cup</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result win">21:20</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/as-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/a25c884d950a539b9a54e2f79206ebe06237-210x178-50x50.jpg">
                                </div>
                                <div class="date">
                                    <strong>Skadowitche 16</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result">Upcoming</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/as-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/lK1NsvDK-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>GO4CS:GO Europe cup #112</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result win">35:28</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/as-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/lK1NsvDK-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>Best of 3</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result lose">0:25</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/as-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/Avatar_Large-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>ESL SUN</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result">Upcoming</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/logo-helmet-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/cloud9-counterstrike-wallpaper-210x178-50x50.jpg">
                                </div>
                                <div class="date">
                                    <strong>D2 Open Sept</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                    </ul>

                </div>
                <div id="g2" class="tab-pane fade">
                    <!-- matches list -->
                    <ul class="match-list">

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result">Upcoming</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/qN1JFnkg-50x50.png">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/cloud9-counterstrike-wallpaper-210x178-50x50.jpg">
                                </div>
                                <div class="date">
                                    <strong>Dawn of the august</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result win">18:12</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/as-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/lK1NsvDK-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>Asia open 2016</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result lose">16:17</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/logo-helmet-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/a25c884d950a539b9a54e2f79206ebe06237-210x178-50x50.jpg">
                                </div>
                                <div class="date">
                                    <strong>Friendly</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result lose">11:16</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/as-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/lK1NsvDK-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>WINNER TAKES IT ALL! #8</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result win">33:24</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/as-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/lK1NsvDK-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>Saturday fun</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result win">12:10</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/as-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/cloud9-counterstrike-wallpaper-210x178-50x50.jpg">
                                </div>
                                <div class="date">
                                    <strong>Saturday fun</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result">Upcoming</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/logo-helmet-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/Avatar_Large-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>Best of 5</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result win">21:20</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/as-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/lK1NsvDK-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>MOBA weekend #2</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result lose">10:19</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/qN1JFnkg-50x50.png">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/lK1NsvDK-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>5 vs 5</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result win">43:40</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/cloud9-counterstrike-wallpaper-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/lK1NsvDK-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>SF World Championships</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result win">35:28</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/as-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/a25c884d950a539b9a54e2f79206ebe06237-210x178-50x50.jpg">
                                </div>
                                <div class="date">
                                    <strong>From zero to hero</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result lose">0:25</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/logo-helmet-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/cloud9-counterstrike-wallpaper-210x178-50x50.jpg">
                                </div>
                                <div class="date">
                                    <strong>Saturday fun</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                        <!-- single match -->
                        <li class="single-match-wrapper">
                            <a href="{{url('/match-detail')}}">
                                <span class="result">Upcoming</span>
                                <div class="single-match">
                                    <img src="{{asset('assets')}}/images/cloud9-counterstrike-wallpaper-210x178-50x50.jpg">
                                    <span class="vs">vs</span>
                                    <img src="{{asset('assets')}}/images/lK1NsvDK-210x178-50x50.png">
                                </div>
                                <div class="date">
                                    <strong>D2 Open Sept</strong>
                                    <span>CS:GO - October 4, 2016, 1:58 pm</span>
                                </div>

                            </a>
                        </li>

                    </ul>

                </div>
            </div>
        </div>

    </div>
</section>

@endsection