@extends('components.master')
@section('body_class', 'single-match-page')
@section('content')
@include('components.include.news-ticker')

<!-- main slider -->
<div class="container-fluid no-padding">
    <div class="slider">
        <div class="container">
            <div class="team-a">
                <div class="team-img">
                    <a href="single-team-page.html"><img src="{{asset('assets')}}/images/as-210x178.jpg"></a>
                    <div class="team-result"><span>18</span></div>
                </div>
                <a href="single-team-page.html" class="team-title"><span>Natus Vincere</span></a>
                <div class="clearfix"></div>
            </div>
            <span class="versus">vs</span>
            <div class="team-a team-b">
                <div class="team-img">
                    <a href="single-team-page.html"><img src="{{asset('assets')}}/images/Avatar_Large-210x178.png"></a>
                    <div class="team-result"><span>12</span></div>
                </div>
                <a href="single-team-page.html" class="team-title"><span>team gg</span></a>
            </div>
            <div class="tournament-meta">
                <div class="tournament-info">
                    <strong>ESL Open Cup</strong>
                    <i class="fa fa-calendar"></i>
                    <span>November 17, 2015 10:00 pm</span>
                </div>
            </div>
        </div>
    </div>
    <div class="top-divider"></div>
</div>

<!-- page content wrapper -->
<section class="content-wrapper container-fluid no-padding">

    <div class="container">
        <div class="main-content col-lg-6">
            <!-- content title -->
            <div class="main-content-title">
                <h3><i class="fa fa-bullhorn"></i> match description</h3>
            </div>
            <div class="blog-content">
                <p>Maecenas ut erat scelerisque tortor vestibulum pretium. Class aptent taciti sociosqu ad litora
                    torquent per conubia nostra, per inceptos himenaeos. Sed iaculis dui vitae nisl lobortis eu vehicula
                    lacus dignissim.</p>
                <blockquote>
                    <p>Donec quis nisi nec nulla malesuada scelerisque vitae ac turpis. In eget elit eu risus gravida
                        adipiscing.</p>
                </blockquote>
                <p>Morbi vel ipsum vel augue mattis ultricies non et mauris. Phasellus rhoncus euismod massa. Etiam
                    euismod tristique venenatis. Curabitur lorem mauris, dictum et tempus eu, feugiat pharetra sapien.
                    Donec vel turpis orci, ut congue tortor. Aenean sed interdum quam. Vivamus laoreet posuere pharetra.
                    In lobortis tellus non augue tempor eleifend. Vestibulum ut dignissim tellus. Morbi ornare, enim a
                    semper venenatis, odio justo luctus enim, consectetur sodales justo est id leo.</p>

            </div>
        </div>

        <div class="sidebar col-lg-6">
            <!-- content title -->
            <div class="main-content-title">
                <h3><i class="fa fa-picture-o"></i> maps</h3>
            </div>

            <ul class="sidebar-maps">
                <li>
                    <strong>Office</strong>
                    <div class="score">
                        <span>11:4</span>
                        <span>7:8</span>
                    </div>
                    <div class="clearfix"></div>
                </li>
            </ul>

            <!-- content title -->
            <div class="main-content-title">
                <h3><i class="fa fa-comments"></i></i> match comments</h3>
            </div>

            <form id="commentform">

                <div class="form-section">
                    <input id="author" name="author" placeholder="Name*" type="text" value="" size="30" maxlength="20"
                        tabindex="3">
                </div>
                <div class="form-section">
                    <input placeholder="Email*" id="email" name="email" type="text" value="" size="30" maxlength="50"
                        tabindex="4">
                </div>
                <div class="form-section">
                    <input id="url" placeholder="Website" name="url" type="text" value="" size="30" maxlength="50"
                        tabindex="5">
                </div>
                <div class="form-section">
                    <textarea placeholder="Leave a comment..." id="comment" name="comment" cols="45" rows="8"
                        tabindex="6"></textarea>
                </div>
                <div class="form-submit">
                    <input id="submit" name="submit" class="button-small button-green" type="submit"
                        value="Submit comment" tabindex="7">
                </div>
            </form>
        </div>
    </div>

</section>

@endsection