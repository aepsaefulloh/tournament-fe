<!-- top header -->
<div class="navbar-wrapper container-fluid">

    <div class="col-lg-4">
        <div class="social-top">
            <a class="rss" href="index.html" target="_blank"> <i class="fa fa-rss"></i></a>
            <a class="youtube" href="index.html" target="_blank"><i class="fa fa-youtube"></i></a>
            <a class="twitter" href="index.html" target="_blank"><i class="fa fa-twitter"></i></a>
            <a class="facebook" href="index.html" target="_blank"><i class="fa fa-facebook"></i></a>
        </div>
    </div>
    <div class="logo col-lg-4 col-md-4">
        <a class="brand" href="{{ url('/') }}"><img alt="logo"
                src="{{ asset('assets') }}/images/logo.png"></a>
    </div>
    <div class="login-info">
        @if (isset($_SESSION) && isset($_SESSION['IS_LOGIN']) && $_SESSION['IS_LOGIN'] == 1)
            <a class="login-btn"><img src="{{ strpos($_SESSION['IMAGE'], 'https') !== false ? $_SESSION['IMAGE'] : env('ASSET_URI') . '/' . $_SESSION['IMAGE'] }}"
                    alt="avatar">
                <span>{{ $_SESSION['USERNAME'] }}</span></a>
            <div class="login-dialog-wrapper is_login">
                <div id="login-dialog" class="login-dialog">
                    <div class="form-wrapper">
                        <i class="fa fa-close"></i>
                        <div class="widget-button">
                            <a href="#">
                                Pengaturan
                            </a>
                        </div>
                        <div class="widget-button">
                            <a href="#">
                                Bantuan
                            </a>
                        </div>
                        <div class="widget-button">
                            <form id="logoutForm" action="{{ route('logout') }}" method="post">
                                @csrf
                                <a href="#" onclick="document.querySelector('#logoutForm').submit()">
                                    Keluar <span><i class="fa fa-sign-out"></i></span>
                                </a>
                            </form>
                        </div>

                    </div>

                </div>
                <div id="mcttCo"></div>
            </div>
        @else
            <a class="register-btn" href="{{ url('/register') }}"><i class="fa fa-pencil-square-o"></i>
                <span>Register</span></a>
            <i>or</i>
            <a class="login-btn"><i class="fa fa-lock"></i> <span>Sign in</span></a>
            <div class="login-dialog-wrapper">
                <div id="login-dialog" class="login-dialog">
                    <div class="form-wrapper">
                        <i class="fa fa-close"></i>
                        <form action="{{ route('login') }}" method="POST" class="login-form">
                            @csrf
                            <input type="text" name="username" class="login" placeholder="Username">
                            <input type="password" name="password" class="login" placeholder="Password">
                            <input type="checkbox" value="forever">
                            <label>Remember Me</label>
                            <a id="lost-password" href="{{ url('/forget-password') }}">Lost your password?</a>
                            <button class="button-medium btn-block" type="submit">GO</button>
                        </form>
                        <a class="register-link" href="{{ url('/register') }}"> Sign up </a>
                    </div>
                    <div class="social-login">
                        <div class="connect-with">Connect with:</div>
                        <div class="google"><a href="{{ route('auth.redirect.google') }}"><i
                                    class="fa fa-google-plus-square"></i></a></div>
                    </div>
                </div>
                <div id="mcttCo"></div>
            </div>
        @endif
    </div>
</div>

<!-- main navigation -->
<nav class="navbar navbar-inverse container-fluid header-navigation-wrapper">
    <div class="container">
        @if (session('success'))
            <div class="alert alert-success">
                <p>{{ session('success') }}</p>
            </div>
        @endif

        @if (session('danger'))
            <div class="alert alert-danger">
                <p>{{ session('danger') }}</p>
            </div>
        @endif
        <div class="navbar-header">
            <button type="button" class="collapsed navbar-toggle" data-toggle="collapse" data-target="#header-menu"
                aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="header-menu">
            <ul class="nav navbar-nav header-menu-navigation">
                <li>
                    <a href="{{ url('/') }}" class="no-dropdown">
                        <i class="header-menu-icon fa fa-home"></i>
                        <span class="header-menu-text">Homepage</span>
                    </a>
                </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" role="button"
                        aria-haspopup="true" aria-expanded="false"><i class="header-menu-icon fa fa-trophy"></i><span
                            class="header-menu-text">team wars</span><i
                            class="dropdown-icon fa fa-angle-down"></i></a>
                    <ul class="dropdown-menu team-wars-menu no-padding">
                        <li class="tw-1">
                            <a href="tournaments-page.html">Tournaments</a>
                            <ul class="tournaments-menu">
                                <li><a href="tournaments-page.html">How it works</a></li>
                                <li><a href="single-tournament.html">Upcoming</a></li>
                                <li><a href="single-tournament-finished.html">Finished</a></li>
                            </ul>
                        </li>
                        <li class="tw-1">
                            <a href="{{ url('/match') }}">Matches</a>
                            <ul class="matches-menu">
                                <li><a href="{{ url('/match') }}">Matches</a></li>
                                <li><a href="upcoming-match.html">Upcoming</a></li>
                                <li><a href="{{ url('/match-detail') }}">Finished</a></li>
                            </ul>
                        </li>
                        <li class="tw-1">
                            <a href="{{ url('/member') }}">Members</a>
                            <ul class="members-menu">
                                <li><a href="{{ url('/member') }}">All Members</a></li>
                            </ul>
                        </li>
                        <li class="tw-1">
                            <a href="{{ url('/team') }}">Teams</a>
                            <ul class="teams-menu">
                                <li><a href="{{ url('/team') }}">All Teams</a></li>
                            </ul>
                        </li>
                        <li class="tw-2">
                            <div>
                                <h4>Create teams, fight matches and organize tournaments</h4>
                            </div>
                        </li>
                        <li class="tw-3">
                            <ul class="tournaments-menu tw3-menu nav nav-tabs">
                                <li class="active">
                                    <a data-toggle="tab" href="#teams" role="button">Teams<i
                                            class="fa fa-angle-right"></i></a>
                                </li>
                                <li><a data-toggle="tab" href="#tournaments" role="button">Tournaments<i
                                            class="fa fa-angle-right"></i></a></li>
                                <li><a data-toggle="tab" href="#matches" role="button">Matches<i
                                            class="fa fa-angle-right"></i></a></li>
                                <li><a data-toggle="tab" href="#members" role="button">Members<i
                                            class="fa fa-angle-right"></i></a>

                                </li>

                                <div class="tab-content">

                                    <ul class="tab-pane active no-padding" id="teams">
                                        <li><a href="{{ url('/team-detail') }}"><img
                                                    src="{{ asset('assets') }}/images/cloud9-counterstrike-wallpaper-1-150x150.jpg"
                                                    alt="cloud9"></a></li>
                                        <li><a href="{{ url('/team-detail') }}"><img
                                                    src="{{ asset('assets') }}/images/lK1NsvDK-1-150x150.png"
                                                    alt="flipside"></a></li>
                                        <li><a href="{{ url('/team-detail') }}"><img
                                                    src="{{ asset('assets') }}/images/a25c884d950a539b9a54e2f79206ebe06237-1-150x150.jpg"
                                                    alt="virtuspro"></a></li>
                                        <li><a href="{{ url('/team-detail') }}"><img
                                                    src="{{ asset('assets') }}/images/as-1-150x150.jpg"
                                                    alt="nv"></a>
                                        </li>
                                        <li><a href="{{ url('/team-detail') }}"><img
                                                    src="{{ asset('assets') }}/images/logo-helmet-1-150x150.jpg"
                                                    alt="skywarrior"></a>
                                        </li>
                                        <li><a href="{{ url('/team-detail') }}"><img
                                                    src="{{ asset('assets') }}/images/qN1JFnkg-1-150x150.png"
                                                    alt="fnatic"></a></li>
                                        <li><a href="{{ url('/team-detail') }}"><img
                                                    src="{{ asset('assets') }}/images/Avatar_Large-1-150x150.png"
                                                    alt="teamgg"></a></li>
                                    </ul>

                                    <ul class="tab-pane no-padding" id="tournaments">
                                        <li id="main-menu-tournaments">

                                            <h4><i style="color: #ff8400" class="fa fa-trophy"></i> LATEST CREATED
                                                TOURNAMENTS</h4>

                                            <!-- tournment slider -->
                                            <div class="carousel slide main-menu-tournament-slider"
                                                data-ride="carousel" id="main-menu-carousel">
                                                <!-- Wrapper for slides -->
                                                <div class="carousel-inner" role="listbox">

                                                    <!-- single tournament -->
                                                    <div class="item active carousel-tournament">
                                                        <div class="single-tournament">
                                                            <div class="carousel-tournament-info">
                                                                <a href="single-tournament.html">
                                                                    <h4>esl 2016</h4>
                                                                </a>
                                                                <div class="tournament-image">
                                                                    <a href="single-tournament.html"><img
                                                                            src="{{ asset('assets') }}/images/cs-85x116.jpg"></a>
                                                                </div>
                                                                <div class="tournament-text">
                                                                    <p><span>Location: </span>Paris, France</p>
                                                                    <br>
                                                                    <p><span>Maximum participants:</span> 100 teams
                                                                    </p>
                                                                    <p><span>Current participants:</span> 4 teams
                                                                    </p>
                                                                    <br>
                                                                    <p class="tournament-prize"><span>Prize:</span>
                                                                        $5000, $2500, $1000, CS:GO T-Shirt</p>
                                                                </div>
                                                            </div>
                                                            <div class="tournament-date">
                                                                <p>May 5, 2016, <span>9:00 pm</span></p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- single tournament -->
                                                    <div class="item carousel-tournament">
                                                        <div class="single-tournament">
                                                            <div class="carousel-tournament-info">
                                                                <a href="single-tournament.html">
                                                                    <h4>LOL RANDOMZ @ COMIC CON</h4>
                                                                </a>
                                                                <div class="tournament-image">
                                                                    <a href="single-tournament.html"><img
                                                                            src="{{ asset('assets') }}/images/lol-85x116.jpg"></a>
                                                                </div>
                                                                <div class="tournament-text">
                                                                    <p><span>Location: </span>Server: 99.168.0.1</p>
                                                                    <br>
                                                                    <p><span>Maximum participants:</span> 128 teams
                                                                    </p>
                                                                    <p><span>Current participants:</span> 4 teams
                                                                    </p>
                                                                    <br>
                                                                    <p class="tournament-prize"><span>Prize:</span>
                                                                        $5000, $2500, $1000, CS:GO T-Shirt</p>
                                                                </div>
                                                            </div>
                                                            <div class="tournament-date">
                                                                <p>May 5, 2016, <span>9:00 pm</span></p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- single tournament -->
                                                    <div class="item carousel-tournament">
                                                        <div class="single-tournament">
                                                            <div class="carousel-tournament-info">
                                                                <a href="single-tournament.html">
                                                                    <h4>ELECTRONIC SPORTS WORLD CUP 2018</h4>
                                                                </a>
                                                                <div class="tournament-image">
                                                                    <a href="single-tournament.html"><img
                                                                            src="{{ asset('assets') }}/images/dota-85x116.jpg"></a>
                                                                </div>
                                                                <div class="tournament-text">
                                                                    <p><span>Location: </span>Paris, France</p>
                                                                    <br>
                                                                    <p><span>Maximum participants:</span> 48 teams
                                                                    </p>
                                                                    <p><span>Current participants:</span> 4 teams
                                                                    </p>
                                                                    <br>
                                                                    <p class="tournament-prize"><span>Prize:</span>
                                                                        $5000, $2500, $1000, CS:GO T-Shirt</p>
                                                                </div>
                                                            </div>
                                                            <div class="tournament-date">
                                                                <p>May 5, 2016, <span>9:00 pm</span></p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <!-- Left and right controls -->
                                                <a class="left carousel-control" href="#main-menu-carousel"
                                                    role="button" data-slide="prev">
                                                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="right carousel-control" href="#main-menu-carousel"
                                                    role="button" data-slide="next">
                                                    <span class="fa fa-chevron-right" aria-hidden="true"></span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>

                                    <ul class="tab-pane no-padding" id="matches">
                                        <li id="main-menu-matches">
                                            <!-- matches -->
                                            <div class="tab-content-wrapper">
                                                <ul class="nav nav-tabs matches-tab-wrapper">
                                                    <li class="active"><a data-toggle="tab" href="#home">ALL</a>
                                                    </li>
                                                    <li><a data-toggle="tab" href="#menu1">CS:GO</a></li>
                                                    <li><a data-toggle="tab" href="#menu2">LOL</a></li>
                                                    <li><a data-toggle="tab" href="#menu3">Battlefield</a></li>
                                                    <li><a data-toggle="tab" href="#menu4">WoT</a></li>
                                                </ul>

                                                <div class="tab-content">
                                                    <div id="home" class="tab-pane fade in active">

                                                        <!-- matches list -->
                                                        <ul class="match-list">

                                                            <!-- single match -->
                                                            <li class="single-match-wrapper">
                                                                <a href="{{ url('/match-detail') }}">
                                                                    <span class="result win">23:20</span>
                                                                    <div class="single-match">
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/qN1JFnkg-25x25.png">
                                                                        <span class="vs">vs</span>
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/cloud9-counterstrike-wallpaper-25x25.jpg">
                                                                    </div>
                                                                    <span class="date">CS:GO - October 4,
                                                                        2016, 1:58
                                                                        pm</span>
                                                                </a>
                                                            </li>

                                                            <!-- single match -->
                                                            <li class="single-match-wrapper">
                                                                <a href="{{ url('/match-detail') }}">
                                                                    <span class="result lose">12:16</span>
                                                                    <div class="single-match">
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/logo-helmet-25x25.jpg">
                                                                        <span class="vs">vs</span>
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/as-25x25.jpg">
                                                                    </div>
                                                                    <span class="date">CS:GO - May 5, 2016,
                                                                        9:00
                                                                        pm</span>
                                                                </a>
                                                            </li>

                                                            <!-- single match -->
                                                            <li class="single-match-wrapper">
                                                                <a href="{{ url('/match-detail') }}">
                                                                    <span class="result">0:0</span>
                                                                    <div class="single-match">
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/13-1-25x25.jpg">
                                                                        <span class="vs">vs</span>
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/20-1-25x25.jpg">
                                                                    </div>
                                                                    <span class="date">CS:GO - May 12, 2016,
                                                                        9:00
                                                                        pm</span>
                                                                </a>
                                                            </li>

                                                            <!-- single match -->
                                                            <li class="single-match-wrapper">
                                                                <a href="{{ url('/match-detail') }}">
                                                                    <span class="result win">43:34</span>
                                                                    <div class="single-match">
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/16-25x25.jpg">
                                                                        <span class="vs">vs</span>
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/19-1-25x25.jpg">
                                                                    </div>
                                                                    <span class="date">CS:GO - October 4,
                                                                        2016, 1:58
                                                                        pm</span>
                                                                </a>
                                                            </li>

                                                            <!-- single match -->
                                                            <li class="single-match-wrapper">
                                                                <a href="{{ url('/match-detail') }}">
                                                                    <span class="result win">24:22</span>
                                                                    <div class="single-match">
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/qN1JFnkg-25x25.png">
                                                                        <span class="vs">vs</span>
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/logo-helmet-25x25.jpg">
                                                                    </div>
                                                                    <span class="date">LoL - April 8, 2016,
                                                                        8:10
                                                                        pm</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div id="menu1" class="tab-pane fade">
                                                        <!-- matches list -->
                                                        <ul class="match-list">
                                                            <!-- single match -->
                                                            <li class="single-match-wrapper">
                                                                <a href="{{ url('/match-detail') }}">
                                                                    <span class="result lose">12:16</span>
                                                                    <div class="single-match">
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/logo-helmet-25x25.jpg">
                                                                        <span class="vs">vs</span>
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/as-25x25.jpg">
                                                                    </div>
                                                                    <span class="date">CS:GO - May 5, 2016,
                                                                        9:00
                                                                        pm</span>
                                                                </a>
                                                            </li>

                                                            <!-- single match -->
                                                            <li class="single-match-wrapper">
                                                                <a href="{{ url('/match-detail') }}">
                                                                    <span class="result win">43:34</span>
                                                                    <div class="single-match">
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/16-25x25.jpg">
                                                                        <span class="vs">vs</span>
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/19-1-25x25.jpg">
                                                                    </div>
                                                                    <span class="date">CS:GO - October 4,
                                                                        2016, 1:58
                                                                        pm</span>
                                                                </a>
                                                            </li>

                                                            <!-- single match -->
                                                            <li class="single-match-wrapper">
                                                                <a href="{{ url('/match-detail') }}">
                                                                    <span class="result win">33:14</span>
                                                                    <div class="single-match">
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/as-25x25.jpg">
                                                                        <span class="vs">vs</span>
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/cloud9-counterstrike-wallpaper-25x25.jpg">
                                                                    </div>
                                                                    <span class="date">LoL - April 8, 2016,
                                                                        8:10
                                                                        pm</span>
                                                                </a>
                                                            </li>
                                                    </div>
                                                    <div id="menu2" class="tab-pane fade">
                                                        <!-- matches list -->
                                                        <ul class="match-list">
                                                            <!-- single match -->
                                                            <li class="single-match-wrapper">
                                                                <a href="{{ url('/match-detail') }}">
                                                                    <span class="result lose">25:27</span>
                                                                    <div class="single-match">
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/19-1-25x25.jpg">
                                                                        <span class="vs">vs</span>
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/16-25x25.jpg">
                                                                    </div>
                                                                    <span class="date">LoL - April 8, 2016,
                                                                        8:10
                                                                        pm</span>
                                                                </a>
                                                            </li>

                                                            <!-- single match -->
                                                            <li class="single-match-wrapper">
                                                                <a href="{{ url('/match-detail') }}">
                                                                    <span class="result lose">24:35</span>
                                                                    <div class="single-match">
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/qN1JFnkg-25x25.png">
                                                                        <span class="vs">vs</span>
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/cloud9-counterstrike-wallpaper-25x25.jpg">
                                                                    </div>
                                                                    <span class="date">CS:GO - October 4,
                                                                        2016, 1:58
                                                                        pm</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div id="menu3" class="tab-pane fade">

                                                        <!-- matches list -->
                                                        <ul class="match-list">

                                                            <!-- single match -->
                                                            <li class="single-match-wrapper">
                                                                <a href="{{ url('/match-detail') }}">
                                                                    <span class="result win">23:20</span>
                                                                    <div class="single-match">
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/qN1JFnkg-25x25.png">
                                                                        <span class="vs">vs</span>
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/cloud9-counterstrike-wallpaper-25x25.jpg">
                                                                    </div>
                                                                    <span class="date">CS:GO - October 4,
                                                                        2016, 1:58
                                                                        pm</span>
                                                                </a>
                                                            </li>

                                                            <!-- single match -->
                                                            <li class="single-match-wrapper">
                                                                <a href="{{ url('/match-detail') }}">
                                                                    <span class="result lose">12:16</span>
                                                                    <div class="single-match">
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/logo-helmet-25x25.jpg">
                                                                        <span class="vs">vs</span>
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/as-25x25.jpg">
                                                                    </div>
                                                                    <span class="date">CS:GO - May 5, 2016,
                                                                        9:00
                                                                        pm</span>
                                                                </a>
                                                            </li>

                                                            <!-- single match -->
                                                            <li class="single-match-wrapper">
                                                                <a href="{{ url('/match-detail') }}">
                                                                    <span class="result">0:0</span>
                                                                    <div class="single-match">
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/13-1-25x25.jpg">
                                                                        <span class="vs">vs</span>
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/20-1-25x25.jpg">
                                                                    </div>
                                                                    <span class="date">CS:GO - May 12, 2016,
                                                                        9:00
                                                                        pm</span>
                                                                </a>
                                                            </li>

                                                            <!-- single match -->
                                                            <li class="single-match-wrapper">
                                                                <a href="{{ url('/match-detail') }}">
                                                                    <span class="result win">43:34</span>
                                                                    <div class="single-match">
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/16-25x25.jpg">
                                                                        <span class="vs">vs</span>
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/19-1-25x25.jpg">
                                                                    </div>
                                                                    <span class="date">CS:GO - October 4,
                                                                        2016, 1:58
                                                                        pm</span>
                                                                </a>
                                                            </li>

                                                            <!-- single match -->
                                                            <li class="single-match-wrapper">
                                                                <a href="{{ url('/match-detail') }}">
                                                                    <span class="result win">24:22</span>
                                                                    <div class="single-match">
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/qN1JFnkg-25x25.png">
                                                                        <span class="vs">vs</span>
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/logo-helmet-25x25.jpg">
                                                                    </div>
                                                                    <span class="date">LoL - April 8, 2016,
                                                                        8:10
                                                                        pm</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div id="menu4" class="tab-pane fade">

                                                        <!-- matches list -->
                                                        <ul class="match-list">

                                                            <!-- single match -->
                                                            <li class="single-match-wrapper">
                                                                <a href="{{ url('/match-detail') }}">
                                                                    <span class="result win">23:20</span>
                                                                    <div class="single-match">
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/qN1JFnkg-25x25.png">
                                                                        <span class="vs">vs</span>
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/cloud9-counterstrike-wallpaper-25x25.jpg">
                                                                    </div>
                                                                    <span class="date">CS:GO - October 4,
                                                                        2016, 1:58
                                                                        pm</span>
                                                                </a>
                                                            </li>

                                                            <!-- single match -->
                                                            <li class="single-match-wrapper">
                                                                <a href="{{ url('/match-detail') }}">
                                                                    <span class="result lose">12:16</span>
                                                                    <div class="single-match">
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/logo-helmet-25x25.jpg">
                                                                        <span class="vs">vs</span>
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/as-25x25.jpg">
                                                                    </div>
                                                                    <span class="date">CS:GO - May 5, 2016,
                                                                        9:00
                                                                        pm</span>
                                                                </a>
                                                            </li>

                                                            <!-- single match -->
                                                            <li class="single-match-wrapper">
                                                                <a href="{{ url('/match-detail') }}">
                                                                    <span class="result win">24:22</span>
                                                                    <div class="single-match">
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/qN1JFnkg-25x25.png">
                                                                        <span class="vs">vs</span>
                                                                        <img
                                                                            src="{{ asset('assets') }}/images/logo-helmet-25x25.jpg">
                                                                    </div>
                                                                    <span class="date">LoL - April 8, 2016,
                                                                        8:10
                                                                        pm</span>
                                                                </a>
                                                            </li>

                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                        </li>
                                    </ul>

                                    <ul class="tab-pane no-padding" id="members">
                                        <li id="main-menu-matches">
                                            <!-- matches -->
                                            <div class="tab-content-wrapper">
                                                <ul class="nav nav-tabs matches-tab-wrapper">
                                                    <li class="active"><a data-toggle="tab" href="#home">NEWEST</a>
                                                    </li>
                                                    <li><a data-toggle="tab" href="#menu1">ACTIVE</a></li>
                                                </ul>

                                                <div class="tab-content">
                                                    <div id="home" class="tab-pane fade in active">

                                                        <!-- members list -->
                                                        <ul class="match-list">

                                                            <!-- single member -->
                                                            <li class="single-match-wrapper single-team">
                                                                <div class="team-avatar"><a
                                                                        href="{{ url('/profile') }}"><img
                                                                            src="{{ asset('assets') }}/images/11-1.jpg"></a>
                                                                </div>
                                                                <div class="team-info">
                                                                    <a href="{{ url('/profile') }}"
                                                                        class="team-title">admin</a>
                                                                    <span class="members">active 1 week
                                                                        ago</span>
                                                                </div>
                                                            </li>

                                                            <li class="single-match-wrapper single-team">
                                                                <div class="team-avatar"><a
                                                                        href="{{ url('/profile') }}"><img
                                                                            src="{{ asset('assets') }}/images/16.jpg"></a>
                                                                </div>
                                                                <div class="team-info">
                                                                    <a href="{{ url('/profile') }}"
                                                                        class="team-title">Pyrotek</a>
                                                                    <span class="members">active 1 week
                                                                        ago</span>
                                                                </div>
                                                            </li>

                                                            <li class="single-match-wrapper single-team">
                                                                <div class="team-avatar"><a
                                                                        href="{{ url('/profile') }}"><img
                                                                            src="{{ asset('assets') }}/images/15-1.jpg"></a>
                                                                </div>
                                                                <div class="team-info">
                                                                    <a href="{{ url('/profile') }}"
                                                                        class="team-title">Kyōsuke</a>
                                                                    <span class="members">active 1 week
                                                                        ago</span>
                                                                </div>
                                                            </li>

                                                            <li class="single-match-wrapper single-team">
                                                                <div class="team-avatar"><a
                                                                        href="{{ url('/profile') }}"><img
                                                                            src="{{ asset('assets') }}/images/22-1.jpg"></a>
                                                                </div>
                                                                <div class="team-info">
                                                                    <a href="{{ url('/profile') }}"
                                                                        class="team-title">Byrewthf</a>
                                                                    <span class="members">active 1 week
                                                                        ago</span>
                                                                </div>
                                                            </li>

                                                            <li class="single-match-wrapper single-team">
                                                                <div class="team-avatar"><a
                                                                        href="{{ url('/profile') }}"><img
                                                                            src="{{ asset('assets') }}/images/18-1.jpg"></a>
                                                                </div>
                                                                <div class="team-info">
                                                                    <a href="{{ url('/profile') }}"
                                                                        class="team-title">BurntRaw</a>
                                                                    <span class="members">active 1 week
                                                                        ago</span>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div id="menu1" class="tab-pane fade">
                                                        <p>Some content in menu 1.</p>
                                                    </div>
                                                    <div id="menu2" class="tab-pane fade">
                                                        <p>Some content in menu 2.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>

                                </div>

                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" role="button"
                        aria-haspopup="true" aria-expanded="false"><i
                            class="header-menu-icon fa fa-crosshairs"></i><span class="header-menu-text">gaming
                            news</span><i class="dropdown-icon fa fa-angle-down"></i></a>
                    <ul class="dropdown-menu no-padding team-wars-menu game-news-menu">
                        <li class="tw-3">
                            <ul class="tournaments-menu tw3-menu nav nav-tabs">
                                <li class="active"><a data-toggle="tab" role="button"
                                        href="#adventure-news">Adventure<i class="fa fa-angle-right"></i></a></li>
                                <li><a data-toggle="tab" role="button" href="#shooter-news">Shooters<i
                                            class="fa fa-angle-right"></i></a>

                                </li>
                                <li><a data-toggle="tab" role="button" href="#racing-news">Racing<i
                                            class="fa fa-angle-right"></i></a></li>

                                <div class="tab-content">

                                    <ul id="adventure-news" class="tab-pane active">
                                        <li><a href="single-post.html"><img
                                                    src="{{ asset('assets') }}/images/Untitled-2.jpg"
                                                    alt="unt2"></a>
                                        </li>
                                        <li class="tw-1">
                                            <ul class="tournaments-menu adventure-menu">
                                                <li class="tw-1">
                                                    <a href="blog-style.html">INDIE</a>
                                                    <ul class="tournaments-menu">
                                                        <li><a href="single-post.html">Etiam massa mauris fermentum
                                                                a congue id</a></li>
                                                        <li><a href="single-post.html">Fermentum sit amet congue sit
                                                                amet</a></li>
                                                        <li><a href="single-post.html">Fusce id justo ac lacus
                                                                aliquet lobortis</a></li>
                                                        <li><a href="single-post.html">Morbi vel ipsum vel augue
                                                                mattis ultricies non et mauris</a></li>
                                                    </ul>
                                                </li>
                                                <li class="tw-1">
                                                    <a href="blog-style.html">MMORPG</a>
                                                    <ul class="tournaments-menu">
                                                        <li><a href="single-post.html">Etiam massa mauris fermentum
                                                                a congue id</a></li>
                                                        <li><a href="single-post.html">Fermentum sit amet congue sit
                                                                amet</a></li>
                                                        <li><a href="single-post.html">Fusce id justo ac lacus
                                                                aliquet lobortis</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="tw-1">
                                            <a href="blog-style.html">FREE TO PLAY</a>
                                            <ul class="tournaments-menu">
                                                <li><a href="single-post.html">In lobortis tellus non augue</a></li>
                                                <li><a href="single-post.html">Integer quis orci nisi auguros
                                                        grantos cibilia lorbos</a></li>
                                                <li><a href="single-post.html">Lorem nibh bibendum nisi nipparo ed
                                                        amus</a></li>
                                                <li><a href="single-post.html">Tortor vestibulum pretiums</a></li>
                                                <li><a href="single-post.html">Vestibulum ut dignissim tellus</a>
                                                </li>
                                                <li><a href="single-post.html">Vivamus a lectus mi cara vita tutta
                                                        nouvos dios</a></li>
                                                <li><a href="single-post.html">Vix at eros intellegat sea no
                                                        facer</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <ul id="shooter-news" class="tab-pane no-padding">
                                        <li class="tw-1">
                                            <ul class="tournaments-menu adventure-menu">
                                                <li class="tw-1">
                                                    <a href="blog-style.html">INDIE</a>
                                                    <ul class="tournaments-menu">
                                                        <li><a href="single-post.html">Etiam massa mauris fermentum
                                                                a congue id</a></li>
                                                        <li><a href="single-post.html">Fermentum sit amet congue sit
                                                                amet</a></li>
                                                        <li><a href="single-post.html">Fusce id justo ac lacus
                                                                aliquet lobortis</a></li>
                                                        <li><a href="single-post.html">Morbi vel ipsum vel augue
                                                                mattis ultricies non et mauris</a></li>
                                                    </ul>
                                                </li>
                                                <li class="tw-1">
                                                    <a href="blog-style.html">MMORPG</a>
                                                    <ul class="tournaments-menu">
                                                        <li><a href="single-post.html">Etiam massa mauris fermentum
                                                                a congue id</a></li>
                                                        <li><a href="single-post.html">Fermentum sit amet congue sit
                                                                amet</a></li>
                                                        <li><a href="single-post.html">Fusce id justo ac lacus
                                                                aliquet lobortis</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="single-post.html"><img
                                                    src="{{ asset('assets') }}/images/8-1-250x250.jpg"
                                                    alt="unt3"><span class="shooters-text-title">Eget ultrices
                                                    mauris rhoncus non</span><span class="shooters-text-desc">May 4,
                                                    2013</span></a></li>
                                        <li><a href="single-post.html"><img
                                                    src="{{ asset('assets') }}/images/2-2-250x250.jpg"
                                                    alt="unt4"><span class="shooters-text-title">Eget ultrices
                                                    mauris rhoncus non</span><span class="shooters-text-desc">May 4,
                                                    2013</span></a></li>
                                    </ul>
                                    <ul id="racing-news" class="tab-pane">
                                        <li><a href="single-post.html"><img
                                                    src="{{ asset('assets') }}/images/30-250x250.jpg"
                                                    alt="unt3"><span class="large-text-title">Eget ultrices mauris
                                                    rhoncus non</span></a></li>
                                        <li><a href="single-post.html"><img
                                                    src="{{ asset('assets') }}/images/21-250x250.jpg"
                                                    alt="unt4"><span class="large-text-title">Eget ultrices mauris
                                                    rhoncus non</span></a></li>
                                        <li><a href="single-post.html"><img
                                                    src="{{ asset('assets') }}/images/9-250x250.jpg"
                                                    alt="unt3"><span class="large-text-title">Eget ultrices mauris
                                                    rhoncus non</span></a></li>
                                        <li><a href="single-post.html"><img
                                                    src="{{ asset('assets') }}/images/6-1-250x250.jpg"
                                                    alt="unt4"><span class="large-text-title">Eget ultrices mauris
                                                    rhoncus non</span></a></li>
                                    </ul>
                                </div>

                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="forum.html" class="no-dropdown"><i class="header-menu-icon fa fa-users"></i><span
                            class="header-menu-text">Forums</span></a></li>

                <li class="dropdown"><a href="shop.html"><i class="header-menu-icon fa fa-shopping-cart"></i><span
                            class="header-menu-text">shop</span><i class="dropdown-icon fa fa-angle-down"></i></a>
                    <ul class="dropdown-menu team-wars-menu game-news-menu shop-menu no-padding">
                        <li class="tw-1">
                            <a href="product.html">Awesome products</a>
                            <a href="product.html"><img src="{{ asset('assets') }}/images/poster_2_up.jpg"
                                    alt="unt3"></a>
                            <a href="product.html"><img src="{{ asset('assets') }}/images/T_7_front.jpg"
                                    alt="unt3"></a>
                            <a href="product.html"><img src="{{ asset('assets') }}/images/hoodie_4_front.jpg"
                                    alt="unt3"></a>
                            <a href="product.html"><img src="{{ asset('assets') }}/images/T_5_front.jpg"
                                    alt="unt3"></a>
                            <a href="product.html"><img src="{{ asset('assets') }}/images/hoodie_5_front.jpg"
                                    alt="unt3"></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{ url('/article') }}" class="no-dropdown">
                        <i class="header-menu-icon fa fa-newspaper-o"></i>
                        <span class="header-menu-text">Article</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                        aria-expanded="false"><i class="header-menu-icon fa fa-envelope-o"></i>
                        <span class="header-menu-text">contact</span><i class="dropdown-icon fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu no-padding team-wars-menu contact-menu">
                        <li class="contact-map">
                            <div><a
                                    href="https://maps.google.com/maps?ll=35.689488,139.691706&z=15&t=m&hl=en-US&gl=US&mapclient=apiv3">
                                    <img
                                        src="https://maps.googleapis.com/maps/api/staticmap?center=Brooklyn+Bridge,New+York,NY&zoom=13&size=500x520&maptype=roadmap&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318&markers=color:red%7Clabel:C%7C40.718217,-73.998284&key=AIzaSyDogsDTAfsS0MSjXngILgz5_4aoAkLPwfc"></a>
                            </div>
                        </li>

                        <li class="shortcodes-title">
                            <h4>SHORTCODES & WIDGETS</h4>
                            <p>Add widgets or shortcodes to your menu to completely customize its contents.</p>
                        </li>

                        <li class="menu-contact-form">
                            <form id="commentform">
                                <div class="form-section">
                                    <input id="author" name="author" placeholder="Name*" type="text"
                                        value="" size="30" maxlength="20" tabindex="3">
                                </div>
                                <div class="form-section">
                                    <input placeholder="Email*" id="email" name="email" type="text"
                                        value="" size="30" maxlength="50" tabindex="4">
                                </div>
                                <div class="form-section">
                                    <input id="url" placeholder="Website" name="url" type="text"
                                        value="" size="30" maxlength="50" tabindex="5">
                                </div>
                                <div class="form-section">
                                    <textarea placeholder="Dummy contact form text..." id="comment" name="comment" cols="45" rows="8"
                                        tabindex="6"></textarea>
                                </div>
                                <div class="form-submit">
                                    <input id="submit" name="submit" class="button-small button-green"
                                        type="submit" value="Send" tabindex="7">
                                </div>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
