<div class="container-fluid ticker">
    <div class="container">
        <div class="ticker-title">
            <i class="fa fa-bullhorn"></i><span>&nbsp;BREAKING NEWS</span>
        </div>
        <div class="tickercontainer">
            <div class="mask">
                <ul id="webticker" class="newsticker">
                    <li class="ticker-spacer"></li>
                    <li>This List Item will scroll infintely</li>
                    <li>And this one will follow it</li>
                    <li class="last">Finally when it goes out of screen, it will queue again at the end</li>
                </ul>
                <span class="tickeroverlay-left">&nbsp;</span>
                <span class="tickeroverlay-right">&nbsp;</span>
            </div>
        </div>
        <div class="search-top">
            <form method="get" id="sform" action="http://skywarriorthemes.com/arcane/">
                <input type="search" placeholder="Search for..." autocomplete="off" name="s"
                    style="width: 3px; padding: 10px 20px 8px; border: 0px solid transparent; cursor: pointer; color: rgb(0, 0, 0);">
                <input type="hidden" name="post_type[]" value="post">
                <input type="hidden" name="post_type[]" value="page">
                <i class="fa fa-search"></i>
            </form>
        </div>
    </div>
</div>