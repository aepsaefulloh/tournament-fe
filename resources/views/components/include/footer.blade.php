<footer class="container-fluid">
    <div class="container">

        <!-- footer left column -->
        <div class="col-lg-4">
            <!-- content title -->
            <div class="main-content-title">
                <h3><i class="fa fa-newspaper-o"></i> latest shooters</h3>
            </div>

            <!-- review post list -->
            <ul class="sidebar-review">

                <!-- single review -->
                <li class="single-review">
                    <a href="#">
                        <div class="img">
                            <img alt="img" src="{{asset('assets')}}/images/26-368x148.jpg">
                            <span class="overlay-link"></span>
                        </div>
                    </a>

                    <div class="review-text">
                        <a href="#" class="review-date"><span><i class="fa fa-calendar"></i> October
                                31, 2015</span></a>
                        <a href="#" class="review-title">Vestibulum ut dignissim tellus</a>
                        <div class="review-rating"><b class="role-color"><i class="fa fa-trophy"></i> 4</b>/5</div>
                    </div>

                    <div class="clearfix"></div>
                </li>

                <!-- single review -->
                <li class="single-review">
                    <a href="#">
                        <div class="img">
                            <img alt="img" src="{{asset('assets')}}/images/19-368x148.jpg">
                            <span class="overlay-link"></span>
                        </div>
                    </a>

                    <div class="review-text">
                        <a href="#" class="review-date"><span><i class="fa fa-calendar"></i> October
                                31, 2015</span></a>
                        <a href="#" class="review-title">Feugiat pharetra sapien ec vel turpis
                            orc</a>
                        <div class="review-rating"><b class="adventure-color"><i class="fa fa-trophy"></i> 4</b>/5
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </li>

                <!-- single review -->
                <li class="single-review">
                    <a href="#">
                        <div class="img">
                            <img alt="img" src="{{asset('assets')}}/images/18-368x148.jpg">
                            <span class="overlay-link"></span>
                        </div>
                    </a>

                    <div class="review-text">
                        <a href="#" class="review-date"><span><i class="fa fa-calendar"></i> October
                                31, 2015</span></a>
                        <a href="#" class="review-title">Aenean sed interdum quammus laoreet posuere
                            pharet</a>
                        <div class="review-rating"><b class="shooter-color"><i class="fa fa-trophy"></i> 4</b>/5
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </li>
            </ul>
        </div>

        <!-- footer middle column -->
        <div class="col-lg-4">
            <!-- content title -->
            <div class="main-content-title">
                <h3><i class="fa fa-rss"></i> instagram</h3>
            </div>

            <div class="instagram-wrapper">

                <!-- single instagram image -->
                <a href="javascript:void(0)">
                    <img src="{{asset('assets')}}/images/13715263_596813070502407_1646627604_n.jpg">
                </a>

                <!-- single instagram image -->
                <a href="javascript:void(0)">
                    <img src="{{asset('assets')}}/images/13774591_1754065994811600_491978258_n.jpg">
                </a>

                <!-- single instagram image -->
                <a href="javascript:void(0)">
                    <img src="{{asset('assets')}}/images/13706881_1118469068196774_1972721860_n.jpg">
                </a>

                <!-- single instagram image -->
                <a href="javascript:void(0)">
                    <img src="{{asset('assets')}}/images/13739556_1158304420899822_1228846908_n.jpg">
                </a>

                <!-- single instagram image -->
                <a href="javascript:void(0)">
                    <img src="{{asset('assets')}}/images/13694358_1042322892521584_1150638250_n.jpg">
                </a>

                <!-- single instagram image -->
                <a href="javascript:void(0)">
                    <img src="{{asset('assets')}}/images/13767625_282321932129985_1522248215_n.jpg">
                </a>

                <!-- single instagram image -->
                <a href="javascript:void(0)">
                    <img src="{{asset('assets')}}/images/13649165_621322221381957_97244884_n.jpg">
                </a>

                <!-- single instagram image -->
                <a href="javascript:void(0)">
                    <img src="{{asset('assets')}}/images/13704373_1723626981230696_1287751506_n.jpg">
                </a>

                <!-- single instagram image -->
                <a href="javascript:void(0)">
                    <img src="{{asset('assets')}}/images/13703208_1600088373622343_2027203245_n.jpg">
                </a>
            </div>
        </div>

        <!-- footer right column -->
        <div class="col-lg-4">
            <!-- content title -->
            <div class="main-content-title">
                <h3><i class="fa fa-rss"></i> match carousel</h3>
            </div>

            <!-- next match slider -->
            <div id="footer-carousel" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">

                    <!-- single match -->
                    <div class="item active">
                        <div class="next-match-wrap">
                            <div class="team-image">
                                <img src="{{asset('assets')}}/images/logo-helmet-150x124.jpg">
                            </div>
                            <div class="team-image">
                                <img src="{{asset('assets')}}/images/cloud9-counterstrike-wallpaper-150x124.jpg">
                            </div>
                            <div class="clearfix"></div>
                            <div class="team-names">
                                <div class="home-team">
                                    <span>skywarrior</span>
                                </div>
                                <div class="versus">
                                    VS
                                </div>
                                <div class="away-team">
                                    <span>cloud 9</span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="match-date">
                                5 vs 5 - November 22, 2019 <span>11:45 am</span>
                            </div>
                        </div>
                    </div>

                    <!-- single match -->
                    <div class="item">
                        <div class="next-match-wrap">
                            <div class="team-image">
                                <img src="{{asset('assets')}}/images/lK1NsvDK-150x124.png">
                            </div>
                            <div class="team-image">
                                <img src="{{asset('assets')}}/images/as-150x124.jpg">
                            </div>
                            <div class="clearfix"></div>
                            <div class="team-names">
                                <div class="home-team">
                                    <span>flipsid3</span>
                                </div>
                                <div class="versus">
                                    VS
                                </div>
                                <div class="away-team">
                                    <span>natus vincere</span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="match-date">
                                D2 Open Sept - September 4, 2021 <span>11:45 am</span>
                            </div>
                        </div>
                    </div>

                    <!-- single match -->
                    <div class="item">
                        <div class="next-match-wrap">
                            <div class="team-image">
                                <img src="{{asset('assets')}}/images/Avatar_Large-150x124.png">
                            </div>
                            <div class="team-image">
                                <img src="{{asset('assets')}}/images/a25c884d950a539b9a54e2f79206ebe06237-150x124.jpg">
                            </div>
                            <div class="clearfix"></div>
                            <div class="team-names">
                                <div class="home-team">
                                    <span>team gg</span>
                                </div>
                                <div class="versus">
                                    VS
                                </div>
                                <div class="away-team">
                                    <span>virtus pro</span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="match-date">
                                WINNER TAKES IT ALL! #8 - April 22, 2019 <span>11:45 am</span>
                            </div>

                        </div>
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#footer-carousel" role="button" data-slide="prev">
                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#footer-carousel" role="button" data-slide="next">
                    <span class="fa fa-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</footer>

<div class="container-fluid copyright">
    <div class="container">
        <p>&copy; 2022 Made by <a href="{{url('/')}}">TandingYuk</a> </p>
        <div class="footer-social">
            <a href="#"><i class="fa fa-rss"></i></a>
            <a href="#"><i class="fa fa-youtube"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-facebook"></i></a>
        </div>

    </div>
</div>