<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="canonical" href="@yield('link-cannonical')" />
    <title>{{ (isset(Helper::config_name_apps()['CVALUE'])) ? Helper::config_name_apps()['CVALUE'] : null }} | Create
        Your Own Battlefield</title>
    <meta name="description" content="@yield('meta-desc')">
    <meta name="keywords" content="@yield('meta-keywords')">
    <meta name="author" content="Floothink">
    <link rel="shortcut icon" href="{{asset('assets')}}/images/favicon.png?<?php echo rand()?>" type="image/x-icon">
    <link rel="icon" href="{{asset('assets')}}/images/favicon.png?<?php echo rand()?>" type="image/x-icon">
    <!-- Facebook & Twitter Meta -->
    <meta property="og:title" content="@yield('meta-fb-title')">
    <meta property="og:type" content="website" />
    <meta property="og:description" content="@yield('meta-fb-desc')">
    <meta property="og:image" content="@yield('meta-fb-image')">
    <meta property="og:url" content="@yield('meta-fb-url')">
    <meta name="twitter:card" content="summary_large_image">
    <!-- Bootstrap Core CSS, FontAwesome, Fonts -->
    <link href="{{asset('assets')}}/css/bootstrap.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('assets')}}/css/style.css" rel="stylesheet">
    <link href="{{asset('assets')}}/css/custom.css?v=<?php echo rand()?>" rel="stylesheet">

    <!-- Fixes and mobile -->
    <link href="{{asset('assets')}}/css/bootstrap-fixes.css" rel="stylesheet">
    <!-- LayerSlider stylesheet -->
    <link rel="stylesheet" href="{{asset('assets')}}/layerslider/css/layerslider.css" type="text/css">
    <!-- Important Owl stylesheet -->
    <link rel="stylesheet" href="{{asset('assets')}}/owl-carousel/owl.carousel.css">
    <!-- Default Theme -->
    <link rel="stylesheet" href="{{asset('assets')}}/owl-carousel/owl.theme.css">
    @stack('styles')
</head>

<body class="@yield('body_class')">
    @include('components.include.navbar')
    @yield('content')
    @include('components.include.footer')
    <!-- jQuery -->
    <script src="{{asset('assets')}}/js/jquery-3.1.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous">
    </script>
    <script src="{{asset('assets')}}/owl-carousel/owl.carousel.js"></script>
    <!-- External libraries: jQuery & GreenSock -->
    <script src="{{asset('assets')}}/layerslider/js/greensock.js" type="text/javascript"></script>
    <script src="{{asset('assets')}}/js/jquery.webticker.min.js"></script>

    <!-- LayerSlider script files -->
    <script src="{{asset('assets')}}/layerslider/js/layerslider.transitions.js" type="text/javascript"></script>
    <script src="{{asset('assets')}}/layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript">
    </script>

    <script type="text/javascript">
    // Running the code when the document is ready
    $(document).ready(function() {

        // Calling LayerSlider on the target element
        $('#layerslider').layerSlider({

            responsive: true,
            skinsPath: 'assets/layerslider/skins/'

        });

        // Calling WebTicker on the target element
        var ticker = $('#webticker');
        ticker.webTicker();

        // Back to top scroll script
        $('#scroll').click(function() {
            $("html, body").animate({
                scrollTop: 0
            }, 900);
            return false;
        });

        $("#owl-demo").owlCarousel({
            autoPlay: 3000, //Set AutoPlay to 3 seconds
            items: 5
        });

        var t = jQuery(".login-dialog-wrapper"),
            e = jQuery(".login-btn"),
            r = jQuery(".form-wrapper i.fa-close"),
            o = jQuery(".navbar-wrapper");
        e.click(function() {
            t.fadeTo("fast", 1, function() {
                jQuery(this).css("top", "50%"), o.css("z-index", "1000")
            })
        }), r.on("click", function() {
            t.fadeTo("fast", 0, function() {
                jQuery(this).css("top", "-5000px"), o.css("z-index", "99999999")
            })
        })


    });
    </script>
    @stack('scripts')

</body>

</html>