@extends('components.master')
@section('body_class', 'single-match-page single-team-page custom-profile')
@section('content')

@include('components.include.news-ticker')

<!-- main slider -->
<div class="container-fluid main-slider no-padding">
    <div class="slider">
        <div class="container">
            <div class="team-a team-b">
                <div class="team-img">
                    <a href="profile.html"><img src="{{asset('assets')}}/images/15-1.jpg">
                        <p class="text-center">KYŌSUKE</p>
                    </a>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="top-divider"></div>

<!-- page content wrapper -->
<section class="content-wrapper container-fluid no-padding">

    <div class="container no-padding">
        <div class="main-content col-lg-8">
            <!-- content title -->
            <div class="main-content-title">
                <h3><i class="fa fa-bullhorn"></i>introduction</h3>
            </div>
            <div class="blog-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse egestas rhoncus nisi ut
                    ullamcorper. Aenean facilisis venenatis justo eu rhoncus. Integer facilisis faucibus lacus, non
                    eleifend tellus. Maecenas eu pulvinar tellus. Curabitur mollis urna elit, vitae suscipit quam
                    aliquet ut. Aenean lobortis dui mi, eget accumsan ipsum suscipit egestas. Vivamus ut porttitor erat.
                    Donec hendrerit vulputate condimentum. Nullam sit amet nunc felis. Etiam non magna sit amet leo
                    elementum accumsan sed nec ante. Pellentesque consequat malesuada mollis.</p>
                <blockquote>Nunc tincidunt nec quam nec mattis. Quisque tincidunt ligula nec sem accumsan fermentum.
                    Nullam a volutpat enim. Vestibulum sapien mi, dignissim quis suscipit tempor, interdum sit amet
                    orci.</blockquote>
                <p>Phasellus efficitur dolor nec felis auctor, eu lacinia quam aliquet. Praesent nec rhoncus augue.
                    Donec tempor dolor sit amet dui aliquam elementum. Cras varius laoreet neque in ultrices. Nulla
                    fringilla massa interdum diam vestibulum bibendum. Sed viverra egestas augue et finibus. Fusce id
                    urna magna. Aliquam erat volutpat. Nullam venenatis tempor velit, vel lacinia justo condimentum in.
                    Curabitur mollis urna elit, vitae suscipit quam aliquet ut. Aenean lobortis dui mi, eget accumsan
                    ipsum suscipit egestas. Vivamus ut porttitor erat. Donec hendrerit vulputate condimentum. Nullam sit
                    amet nunc felis. Etiam non magna sit amet leo elementum accumsan sed nec ante. Pellentesque
                    consequat malesuada mollis.</p>
            </div>
        </div>
        <div class="sidebar col-lg-4">

            <!-- content title -->
            <div class="main-content-title">
                <h3><i class="fa fa-info-circle"></i> About</h3>
            </div>

            <ul class="about-profile-list">
                <li>
                    <strong>Name:</strong> Roger Pearl
                </li>

                <li>
                    <strong>location:</strong> Japan, Kyoto
                </li>

                <li>
                    <strong>Age:</strong> 27
                </li>

                <li>
                    <strong>joined:</strong> Aug, 2016
                </li>

                <li>
                    <strong>website:</strong> <a href="http://skywarriorthemes.com">http://skywarriorthemes.com/</a>
                </li>
            </ul>

            <!-- content title -->
            <div class="main-content-title">
                <h3><i class="fa fa-crosshairs"></i> Teams</h3>
            </div>

            <ul class="about-profile-list">
                <li>
                    <a href="single-team-page.html">
                        <div class="about-profile-img-wrapper">
                            <img src="{{asset('assets')}}/images/logo-helmet.jpg">
                        </div>
                        <p>skywarrior</p>
                        <div class="clearfix"></div>
                    </a>
                </li>

                <li>
                    <a href="single-team-page.html">
                        <div class="about-profile-img-wrapper">
                            <img src="{{asset('assets')}}/images/lK1NsvDK.png">
                        </div>
                        <p>flipsid3</p>
                        <div class="clearfix"></div>
                    </a>
                </li>

                <li>
                    <a href="single-team-page.html">
                        <div class="about-profile-img-wrapper">
                            <img src="{{asset('assets')}}/images/qN1JFnkg.png">
                        </div>
                        <p>FNATIC</p>
                        <div class="clearfix"></div>
                    </a>
                </li>
            </ul>

        </div>
    </div>

</section>


@endsection