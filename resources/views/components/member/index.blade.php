@extends('components.master')
@section('body_class', 'blog-style teams-page members-page')
@section('content')


<!-- main slider -->
<div class="container no-padding">
    <div class="slider col-lg-12">
        <h1>Search members</h1>
    </div>
</div>

@include('components.include.news-ticker')

<!-- page content wrapper -->
<section class="content-wrapper container-fluid no-padding">
    <div class="container main-wrapper">

        <div class="all-members">
            <a href="#">All members <span>11</span></a>
        </div>

        <form class="search-teams">
            <input type="text" autocomplete="off" name="team_name" id="team_name" placeholder="Search Teams...">
            <input type="button" id="members_search_submit" name="members_search_submit" value="Search"
                class="button-medium">
        </form>

        <ul class="teams-list">

            <li class="single-team">
                <div class="team-avatar"><a href="{{url('/profile')}}"><img src="{{asset('assets')}}/images/11-1.jpg"></a>
                </div>
                <div class="team-info">
                    <a href="{{url('/profile')}}" class="team-title">admin</a>
                    <span class="members">active 1 week ago</span>
                </div>
            </li>

            <li class="single-team">
                <div class="team-avatar"><a href="{{url('/profile')}}"><img src="{{asset('assets')}}/images/16.jpg"></a></div>
                <div class="team-info">
                    <a href="{{url('/profile')}}" class="team-title">Pyrotek</a>
                    <span class="members">active 1 week ago</span>
                </div>
            </li>

            <li class="single-team">
                <div class="team-avatar"><a href="{{url('/profile')}}"><img src="{{asset('assets')}}/images/15-1.jpg"></a>
                </div>
                <div class="team-info">
                    <a href="{{url('/profile')}}" class="team-title">Kyōsuke</a>
                    <span class="members">active 1 week ago</span>
                </div>
            </li>

            <li class="single-team">
                <div class="team-avatar"><a href="{{url('/profile')}}"><img src="{{asset('assets')}}/images/22-1.jpg"></a>
                </div>
                <div class="team-info">
                    <a href="{{url('/profile')}}" class="team-title">Byrewthf</a>
                    <span class="members">active 1 week ago</span>
                </div>
            </li>

            <li class="single-team">
                <div class="team-avatar"><a href="{{url('/profile')}}"><img src="{{asset('assets')}}/images/18-1.jpg"></a>
                </div>
                <div class="team-info">
                    <a href="{{url('/profile')}}" class="team-title">BurntRaw</a>
                    <span class="members">active 1 week ago</span>
                </div>
            </li>

            <li class="single-team">
                <div class="team-avatar"><a href="{{url('/profile')}}"><img src="{{asset('assets')}}/images/20-1.jpg"></a>
                </div>
                <div class="team-info">
                    <a href="{{url('/profile')}}" class="team-title">FoxSlay</a>
                    <span class="members">active 1 week ago</span>
                </div>
            </li>

            <li class="single-team">
                <div class="team-avatar"><a href="{{url('/profile')}}"><img src="{{asset('assets')}}/images/13-1.jpg"></a>
                </div>
                <div class="team-info">
                    <a href="{{url('/profile')}}" class="team-title">Swagson</a>
                    <span class="members">active 1 week ago</span>
                </div>
            </li>

            <li class="single-team">
                <div class="team-avatar"><a href="{{url('/profile')}}"><img src="{{asset('assets')}}/images/21-1.jpg"></a>
                </div>
                <div class="team-info">
                    <a href="{{url('/profile')}}" class="team-title">Energet</a>
                    <span class="members">active 1 week ago</span>
                </div>
            </li>

            <li class="single-team">
                <div class="team-avatar"><a href="{{url('/profile')}}"><img src="{{asset('assets')}}/images/17-1.jpg"></a>
                </div>
                <div class="team-info">
                    <a href="{{url('/profile')}}" class="team-title">HotShot</a>
                    <span class="members">active 1 week ago</span>
                </div>
            </li>

            <li class="single-team">
                <div class="team-avatar"><a href="{{url('/profile')}}"><img src="{{asset('assets')}}/images/12-1.jpg"></a>
                </div>
                <div class="team-info">
                    <a href="{{url('/profile')}}" class="team-title">Raaaa</a>
                    <span class="members">active 1 week ago</span>
                </div>
            </li>

            <li class="single-team">
                <div class="team-avatar"><a href="{{url('/profile')}}"><img src="{{asset('assets')}}/images/19-1.jpg"></a>
                </div>
                <div class="team-info">
                    <a href="{{url('/profile')}}" class="team-title">g0dspeed</a>
                    <span class="members">active 1 week ago</span>
                </div>
            </li>
        </ul>
        <div class="members-count">
            <span>Viewing 1 - 11 of 11 active members</span>
        </div>

    </div>
</section>

@endsection