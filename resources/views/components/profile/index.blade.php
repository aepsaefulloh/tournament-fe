@extends('components.master')
@section('body_class', 'single-match-page single-team-page custom-profile')

@section('content')
<!-- news ticker -->
<div class="container-fluid ticker">
    <div class="container">
        <div class="ticker-title">
            <i class="fa fa-bullhorn"></i><span>&nbsp;BREAKING NEWS</span>
        </div>
        <div class="tickercontainer">
            <div class="mask">
                <ul id="webticker" class="newsticker">
                    <li class="ticker-spacer"></li>
                    <li>This List Item will scroll infintely</li>
                    <li>And this one will follow it</li>
                    <li class="last">Finally when it goes out of screen, it will queue again at the end</li>
                </ul>
                <span class="tickeroverlay-left">&nbsp;</span>
                <span class="tickeroverlay-right">&nbsp;</span>
            </div>
        </div>
        <div class="search-top">
            <form method="get" id="sform" action="http://skywarriorthemes.com/arcane/">
                <input type="search" placeholder="Search for..." autocomplete="off" name="s" style="width: 3px; padding: 11px 20px 8px; border: 0px solid transparent; cursor: pointer; color: rgb(0, 0, 0);">
                <input type="hidden" name="post_type[]" value="post">
                <input type="hidden" name="post_type[]" value="page">
                <i class="fa fa-search"></i>
            </form>
        </div>
    </div>
</div>
<!-- main slider -->
<div class="container-fluid main-slider no-padding">
    <div class="slider">
        <div class="container">
            <div class="team-a team-b">
                <div class="team-img">
                    <a href="profile.html"><img src="{{$data['IMAGE']}}" data-pagespeed-url-hash="1756154864" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                        <p class="text-center">{{$data['FULLNAME']}}</p>
                    </a>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="top-divider"></div>
<!-- page content wrapper -->
<section class="content-wrapper container-fluid no-padding">

    <div class="container no-padding">
        <div class="main-content col-lg-8">
            <!-- content title -->
            <div class="main-content-title">
                <h3><i class="fa fa-bullhorn"></i>introduction</h3>
            </div>
            <div class="blog-content">
                {{$data['description']}}
            </div>
        </div>
        <div class="sidebar col-lg-4">

            <!-- content title -->
            <div class="main-content-title">
                <h3><i class="fa fa-info-circle"></i> About</h3>
            </div>

            <ul class="about-profile-list">
                <li>
                    <strong>Name:</strong> {{$data['FULLNAME']}}
                </li>

                <li>
                    <strong>location:</strong> Japan, Kyoto
                </li>

                <li>
                    <strong>Age:</strong> 27
                </li>

                <li>
                    <strong>joined:</strong> Aug, 2016
                </li>

                <li>
                    <strong>website:</strong> <a href="http://skywarriorthemes.com">http://skywarriorthemes.com/</a>
                </li>
            </ul>

            <!-- content title -->
            <div class="main-content-title">
                <h3><i class="fa fa-crosshairs"></i> Teams</h3>
            </div>

            <ul class="about-profile-list">
                <li>
                    <a href="single-team-page.html">
                        <div class="about-profile-img-wrapper">
                            <img src="images/xlogo-helmet.jpg.pagespeed.ic._rSf2IMt5U.webp" data-pagespeed-url-hash="878351877" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                        </div>
                        <p>skywarrior</p>
                        <div class="clearfix"></div>
                    </a>
                </li>

                <li>
                    <a href="single-team-page.html">
                        <div class="about-profile-img-wrapper">
                            <img src="images/xlK1NsvDK.png.pagespeed.ic.mxvfmQs9bi.png" data-pagespeed-url-hash="1451132566" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                        </div>
                        <p>flipsid3</p>
                        <div class="clearfix"></div>
                    </a>
                </li>

                <li>
                    <a href="single-team-page.html">
                        <div class="about-profile-img-wrapper">
                            <img src="images/xqN1JFnkg.png.pagespeed.ic.MpSJprY9sr.png" data-pagespeed-url-hash="3671190302" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                        </div>
                        <p>FNATIC</p>
                        <div class="clearfix"></div>
                    </a>
                </li>
            </ul>

        </div>
    </div>

</section>

@endsection