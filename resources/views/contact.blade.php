@extends('components.master')

@section('content')
<section class="section-contact-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="heading w-100">
                    <h2 class="title text-outline-white text-uppercase">Contact</h2>
                </div>
                <div class="row mt-3">
                    <div class="col-md-8">
                        <div class="nav-text">
                            <h6>
                                <a
                                    href="#">{{ (isset(Helper::config_address()['CVALUE'])) ? Helper::config_address()['CVALUE'] : null }}</a>
                            </h6>

                            <h3 class="mt-5">
                                <a
                                    href="tel:{{ (isset(Helper::config_phone()['CVALUE'])) ? Helper::config_phone()['CVALUE'] : null }}">{{ (isset(Helper::config_phone()['CVALUE'])) ? Helper::config_phone()['CVALUE'] : null }}</a>
                            </h3>
                            <h3>
                                <a
                                    href="mailto:{{ (isset(Helper::config_email()['CVALUE'])) ? Helper::config_email()['CVALUE'] : null }}">{{ (isset(Helper::config_email()['CVALUE'])) ? Helper::config_email()['CVALUE'] : null }}</a>
                            </h3>
                        </div>
                        <div class="social-media-text">
                            <ul>
                                <li><a href="">facebook</a></li>
                                <li><a href="">twitter</a></li>
                                <li><a href="">instagram</a></li>
                                <li><a href="">linkedin</a></li>
                            </ul>
                        </div>

                    </div>

                </div>
            </div>
            <div class="col-md-6 align-self-center">
                <div class="form-contact">
                    <form>
                        <div class="mb-4">
                            <input type="text" class="form-control" placeholder="Name" value="" />
                        </div>
                        <div class="mb-4">
                            <input type="text" class="form-control" placeholder="Email" value="" />
                        </div>
                        <div class="mb-4">
                            <textarea class="form-control" rows="3" placeholder="Message" value=""></textarea>
                        </div>

                        <button type="submit" class="btn btn-white">Submit <img
                                src="{{asset('assets')}}/images/vector/arrow-up-right.svg" class="img-fluid ms-5" alt=""
                                width="15"></button>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="line"> </div>
                <div class="grid-contact-information">
                    <div class="grid-item">
                        <div class="contact-widget-title">
                            General & Business
                        </div>
                        <div class="caption">
                            For new business inquiries, send your project brief to the email below and we will get back
                            to
                            you soon. And we’re not shying away from collaborations or community project, so send us
                            your
                            proposal also to this email
                        </div>

                        <div class="link">
                            <a href="">info@Idealive.asia</a>
                        </div>
                    </div>
                    <div class="grid-item">
                        <div class="contact-widget-title">
                            Jobs
                        </div>
                        <div class="caption">
                            For new business inquiries, send your project brief to the email below and we will get back
                            to
                            you soon. And we’re not shying away from collaborations or community project, so send us
                            your
                            proposal also to this email
                        </div>

                        <div class="link">
                            <a href="">info@Idealive.asia</a>
                        </div>
                    </div>
                    <div class="grid-item">
                        <div class="contact-widget-title">
                            Intership
                        </div>
                        <div class="caption">
                            For new business inquiries, send your project brief to the email below and we will get back
                            to
                            you soon. And we’re not shying away from collaborations or community project, so send us
                            your
                            proposal also to this email
                        </div>

                        <div class="link">
                            <a href="">info@Idealive.asia</a>
                        </div>
                    </div>
                    <div class="grid-item">
                        <div class="contact-widget-title">
                            Workshop And Talks
                        </div>
                        <div class="caption">
                            For new business inquiries, send your project brief to the email below and we will get back
                            to
                            you soon. And we’re not shying away from collaborations or community project, so send us
                            your
                            proposal also to this email
                        </div>

                        <div class="link">
                            <a href="">info@Idealive.asia</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
@endsection