<?php
namespace App\Helper;

use Illuminate\Support\Facades\Http;

class Helper {
    public static function url_slug($url) {
        return preg_replace('/[^\p{L}\p{N}]/u', '-' , $url);
    }

    // -------------------------- get api config apps -------------------------------- //
    public static function get_config_apps() {
        $http_get = Http::withHeaders([
            'user_agent'=> $_SERVER['HTTP_USER_AGENT'],
            'token'     => '$2a$12$8r8NRGqWuxZuMsMz9HarMOgWzgEZDQ7NViaw309mzOsCOfK90OWme'
        ])->get('https://staging.sketsahouse.com/tournament/api/public/api/setting-apps');
        // dd($http_get->body());
        if ($http_get->ok()) {
            return $http_get['data'];
        } elseif ($http_get['status'] == 401) {
            return [];
        } else {
            return [];
        }  
    }

    public static function config_logo() {
        $data = Helper::get_config_apps();
        if ($data && count($data) > 0) {
            return collect($data)->where('LABEL', 'Logo')->first();
        } else {
            return null;
        }
    }

    public static function config_phone() {
        $data = Helper::get_config_apps();
        if ($data && count($data) > 0) {
            return collect($data)->where('LABEL', 'phone')->first();
        } else {
            return null;
        }
    }

    public static function config_email() {
        $data = Helper::get_config_apps();
        if ($data && count($data) > 0) {
            return collect($data)->where('LABEL', 'email')->first();
        } else {
            return null;
        }
    }

    public static function config_address() {
        $data = Helper::get_config_apps();
        if ($data && count($data) > 0) {
            return collect($data)->where('LABEL', 'Address')->first();
        } else {
            return null;
        }
    }

    public static function config_name_apps() {
        $data = Helper::get_config_apps();
        if ($data && count($data) > 0) {
            return collect($data)->where('LABEL', 'Sitename')->first();
        } else {
            return null;
        }
    }

    public static function config_desc() {
        $data = Helper::get_config_apps();
        if ($data && count($data) > 0) {
            return collect($data)->where('LABEL', 'Description')->first();
        } else {
            return null;
        }
    }

    public static function config_instagram() {
        $data = Helper::get_config_apps();
        if ($data && count($data) > 0) {
            return collect($data)->where('LABEL', 'Instagram')->first();
        } else {
            return null;
        }
    }

    public static function config_facebook() {
        $data = Helper::get_config_apps();
        if ($data && count($data) > 0) {
            return collect($data)->where('LABEL', 'Facebook')->first();
        } else {
            return null;
        }
    }
    public static function config_twitter() {
        $data = Helper::get_config_apps();
        if ($data && count($data) > 0) {
            return collect($data)->where('LABEL', 'Twitter')->first();
        } else {
            return null;
        }
    }
    public static function config_linkedin() {
        $data = Helper::get_config_apps();
        if ($data && count($data) > 0) {
            return collect($data)->where('LABEL', 'Linkedin')->first();
        } else {
            return null;
        }
    }
}