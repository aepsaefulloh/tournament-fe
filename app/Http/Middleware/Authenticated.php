<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Authenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        session_start();
        try {
            if ($_SESSION['IS_LOGIN'] && $_SESSION['TOKEN']) {
                return $next($request);
            }else{
                return abort('401');
            }
        } catch (\Throwable $th) {
            return abort('401');
        }
    }
}
