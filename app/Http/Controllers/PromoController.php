<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PromoController extends Controller
{
    public function __construct() {
        session_start();
    }
    public function index(Request $request){


        $promo = $this->http_get($this->url_api().'content?category=13');
        // dd($promo['data']);
        if ($promo && $promo['status'] == 200) {
            $this->data['promo'] = $promo['data'];
        } else {
            $this->data['promo'] = [];
        }

        return view('promo', $this->data);
    }
}