<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Helper;


class TournamentController extends Controller
{
    public function __construct()
    {
        session_start();
    }

    public function upcoming()
    {
        // Data content Find
        $tournament = $this->http_get($this->url_api() . 'tournament');
        if (isset($tournament['data'])) {

            $this->data['data'] = $tournament['data'];
        } else {
            $this->data['data'] = [];
        }
        // dd($tournament['data']);
        return view('components.tournament.upcoming', $this->data);
    }

    public function detail($id)
    {
        $this->data['id'] = $id;
        // Data content Find
        $tournament = $this->http_get($this->url_api() . 'tournament/' . $id);
        $this->data['data'] = $tournament['data'];
        // dd($tournament['data']);
        return view('components.tournament.detail', $this->data);
    }

    public function post_comment(Request $request)
    {
        $data = [
            'COMMENT' => $request->comment,
            'IPLOC' => $_SERVER['REMOTE_ADDR'] . "|" . $_SERVER['HTTP_USER_AGENT'],
            "FULLNAME" => $_SESSION['FULLNAME'],
            "EMAIL" => $_SESSION['EMAIL'],
            "CTIMESTAMP" => date('Y-m-d H:i:s'),
            "ARTICLE_ID" => $request->article_id,
        ];
        $http_post = $this->http_post($this->url_api() . 'comment/store', $data);
        if ($http_post['status'] == 200) {

            return redirect()->back();
        } elseif ($http_post['status'] == 422) {
            return redirect()->back()->with('backendErrors', $http_post['backendErrors']);
        } else {
            return redirect()->back();
        }
    }

    private function get_comment($id)
    {
        // Data content
        $content = $this->http_get($this->url_api() . 'comment?id=' . $id);
        if ($content && $content['status'] == 200 && count($content['data']) > 0) {
            $this->data['content'] = $content;
        } else {
            $this->data['content'] = [];
        }

        return $this->data['content'];
    }
}
