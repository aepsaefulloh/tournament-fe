<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function index(){
        return view('components.member.index');
    }

    public function profile(){
        return view('components.member.profile');
    }
}
