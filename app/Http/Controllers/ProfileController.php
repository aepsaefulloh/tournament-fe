<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Helper;
use Session;

class ProfileController extends Controller
{
    public function __construct()
    {
        session_start();
    }

    public function index()
    {
        // dd(Session::get('ID'));
        // Data content Find
        $tournament = $this->http_get($this->url_api() . 'profile?id=' . $_SESSION['ID']);
        if (isset($tournament['data'])) {

            $this->data['data'] = $tournament['data']['data'];
        } else {
            $this->data['data'] = [];
        }
        $this->data['data'] += $_SESSION;
        return view('components.profile.index', $this->data);
    }
}
