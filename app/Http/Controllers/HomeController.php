<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        session_start();
    }

    public function index()
    {
        // Data Content
        $content = $this->http_get($this->url_api() . 'content?category=1&perpage=6');
        if ($content && $content['status'] == 200) {
            $this->data['content'] = $content['data'];
        } else {
            $this->data['content'] = [];
        }

        // Popular Post
        $popular_post = $this->http_get($this->url_api() . 'content?perpage=4');
        if ($popular_post && $popular_post['status'] == 200) {
            $this->data['popular_post'] = $popular_post['data'];
        } else {
            $this->data['popular_post'] = [];
        }

        // Tournament
        $tournament = $this->http_get($this->url_api() . 'tournament?perpage=4');
        if ($tournament && $tournament['status'] == 200) {
            $this->data['tournament'] = $tournament['data'];
        } else {
            $this->data['tournament'] = [];
        }

        // Match
        $match = $this->http_get($this->url_api() . 'match');
        if ($match && $match['status'] == 200) {
            if (isset($match['data'])) {
                $this->data['match'] = $match['data'];
            } else {
                $this->data['match'] = [];
            }
        } else {
            $this->data['match'] = [];
        }
        // dd($this->data['match']);

        // Team
        $team = $this->http_get($this->url_api() . 'team');
        if ($team && $team['code'] == 200) {
            if (isset($team['data'])) {
                $this->data['team'] = $team['data']['data'];
            } else {
                $this->data['team'] = [];
            }
        } else {
            $this->data['team'] = [];
        }

        return view('home', $this->data);
    }
}
