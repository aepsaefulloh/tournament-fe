<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function index()
    {
        // Team
        $team = $this->http_get($this->url_api() . 'team');
        if ($team && $team['code'] == 200) {
            if (isset($team['data'])) {
                $this->data['team'] = $team['data']['data'];
            } else {
                $this->data['team'] = [];
            }
        } else {
            $this->data['team'] = [];
        }
        // dd($this->data);
        return view('components.team.index', $this->data);
    }

    public function detail($id)
    {
        // team
        $team = $this->http_get($this->url_api() . 'team/detail/about/' . $id);
        if ($team && $team['code'] == 200) {
            if (isset($team['data'])) {
                $this->data['team'] = $team['data']['data'];
            } else {
                $this->data['team'] = [];
            }
        } else {
            $this->data['team'] = [];
        }

        // member
        $member = $this->http_get($this->url_api() . 'team/detail/member/' . $id);
        if ($member && $member['code'] == 200) {
            if (isset($member['data'])) {
                $this->data['member'] = $member['data']['data'];
            } else {
                $this->data['member'] = [];
            }
        } else {
            $this->data['member'] = [];
        }
        // dd($this->data['team']);
        return view('components.team.detail', $this->data);
    }

    public function add()
    {
        return view('components.team.add');
    }
}
