<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Rules\WithoutSpace;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);
        
        if($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        
        $data = [
            'username' => $request->username,
            'password' => $request->password
        ];
        
        $http_post = $this->http_post($this->url_api().'login', $data);

        if ($http_post['status'] == 200) {
            session_start();
            $data = $http_post['data'];
            $_SESSION = array_merge($_SESSION, $data);
        
            return redirect()->route('home');
        }elseif($http_post['status'] == 422){
            return redirect()->back()->with('backendErrors', $http_post['backendErrors']);
        }else{
            return redirect()->back()->with('danger', "Your account is not register or not verified");
        }   
    }

    public function registerForm()
    {
        $city = $this->http_get($this->url_api().'city');

        if ($city && $city['status'] == 200) {
            $this->data['city'] = $city['data'];
        } else {
            $this->data['city'] = [];
        }

        return view('components.auth.register', $this->data);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => ['required', new WithoutSpace],
            'password' => 'required',
            'email' => 'required',
            'fullname' => 'required',
            'city_id' => 'required',
        ]);

        if($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $data = [
            'username' => $request->username,
            'password' => $request->password,
            'email' => $request->email,
            'fullname' => $request->fullname,
            'city_id' => $request->city_id,
        ];
        
        $http_post = $this->http_post($this->url_api().'register', $data);

        if($http_post['status'] == 422){
            return redirect()->back()->with('backendErrors', $http_post['backendErrors']);
        }elseif ($http_post->status() == 200) {
            return redirect()->back()->with('success', $http_post['data']['message']);
        }else{
            return redirect()->back();
        }
    }

    public function forget()
    {
        return view('components.auth.forget');
    }

    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
        ]);
        
        if($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        
        $http_get = $this->http_get($this->url_api()."reset-password?token=$request->token");

        if ($http_get['status'] == 200) {
            $data = $http_get['data'];
            $data['token'] = $request->token;
            
            return view('components.auth.reset', $data);
        }else{
            return abort('404');
        }
    }

    public function forgetPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        
        $http_post = $this->http_post($this->url_api().'forgot-password', [
            'email' => $request->email
        ]);

        if ($http_post['status'] == 200) {
            $request->session()->flash('message', "Success Send Email");
            $request->session()->flash('email', $request->email);
            
            return redirect()->back();
        } else {
            return redirect()->back()->with('backendErrors', $http_post['backendErrors']);
        }
    }

    public function resetPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);

        if($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        
        $http_post = $this->http_post($this->url_api().'reset-password', [
            'token' => $request->token,
            'password' => $request->password,
            'password_confirmation' => $request->password_confirmation
        ]);

        if ($http_post['status'] == 200) {
            return redirect()->route('home')->with('success', "Success Reset Password Please Login");
        } else {
            return redirect()->back()->with('backendErrors', $http_post['backendErrors']);
        }
    }

    public function verificateEmail(Request $request)
    {
        $token = $request->token;
        $response = $this->http_post($this->url_api().'verificate-email', [
            "token" => $token
        ]);
        
        if($response->status() == 404){
            return redirect()->route('home');
        }elseif ($response->status() == 200) {
            session_start();
            $data = $response['data'];
            $_SESSION = array_merge($_SESSION, $data);
        
            return redirect()->route('home');
        }else{
            return redirect()->route('home');
        }
    }

    public function logout()
    {
        session_destroy();
        return redirect()->route('home');
    }
}