<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Helper;


class ProjectController extends Controller
{
    public function __construct() {
        session_start();
    }
    public function index(Request $request)
    {
         // Data Project
         $project = $this->http_get($this->url_api().'project?perpage=20');
         if ($project && $project['status'] == 200) {
             $this->data['project'] = $project['data'];
         } else {
             $this->data['project'] = [];
         }
         $category = $this->http_get($this->url_api().'category');

         if ($category && $category['status'] == 200) {
             $this->data['category'] = $category['data'];
         } else {
             $this->data['category'] = [];
         }
        return view('components.project.index', $this->data);
    }

    public function project_category(Request $request, $name){
        $perpage = 15;
        $page = ($request->page != '') ? $request->page : 1;

        $category = $this->http_get($this->url_api().'category/'.ucfirst(str_replace("-", " ", $name)));
        // Data Product
        $project = $this->http_get($this->url_api().'project/category/'.$category['data']['ID']);

        // dd($project['data']);
        if ($project && $project['status'] == 200) {
            $this->data['project'] = $project['data'];
            $this->data['paginate_all'] = ceil($project['links']['parameters']['count_data']/$perpage);
        } else {
            $this->data['project'] = [];
            $this->data['paginate_all'] = 0;
        }
        $category = $this->http_get($this->url_api().'category');

        if ($category && $category['status'] == 200) {
            $this->data['category'] = $category['data'];
        } else {
            $this->data['category'] = [];
        }
        // dd($category['data']);
        
        return view('components.project.index', $this->data);

    }

    public function detail($id, $title){
        // Data content Find
        $project = $this->http_get($this->url_api().'project/'.$id);
        // echo($project);
        if ($project && $project['status'] == 200 && isset($project['data']['IMAGE'])) {
            $this->data['project'] = $project['data'];
            // return view('components.project.detail', $this->data);
        } else {
            return abort(404, 'Page not found');
        }

        $other_project = $this->http_get($this->url_api().'project?perpage=4');
            if ($other_project && $other_project['status'] == 200) {
                $this->data['other_project'] = $other_project['data'];
            } else {
                $this->data['other_project'] = [];
            }
         return view('components.project.detail', $this->data); 
        
    }
}