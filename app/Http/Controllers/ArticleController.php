<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Helper;


class ArticleController extends Controller
{
    public function __construct()
    {
        session_start();
    }

    public function index(Request $request)
    {
        $perpage = 10;
        $page = ($request->page != '') ? $request->page : 1;

        // Data content
        $content = $this->http_get($this->url_api() . 'content?page=' . $page . '&perpage=' . $perpage);
        if ($content && $content['status'] == 200 && count($content['data']) > 0) {
            $this->data['content'] = $content['data'];

            $this->data['paginate_all'] = ceil($content['links']['parameters']['count_data'] / $perpage);
        } else {
            $this->data['content'] = [];

            $this->data['paginate_all'] = 0;
        }
        $this->data['pop_roll_current'] = $content['data'];

        // Latest Post
        $latest_post = $this->http_get($this->url_api() . 'content?page=1&perpage=5');
        if ($latest_post && $latest_post['status'] == 200) {
            $this->data['latest_post'] = $latest_post['data'];
        } else {
            $this->data['latest_post'] = [];
        }

        return view('components.article.index', $this->data);
    }




    public function detail($id, $title)
    {
        $this->data['id'] = $id;
        // Data content Find
        $content = $this->http_get($this->url_api() . 'content/' . $id);
        $comment = $this->get_comment($id);

        // echo($content);
        if ($content && $content['status'] == 200 && isset($content['data']['IMAGE'])) {
            $this->data['content'] = $content['data'];
            if (count($comment) != 0) {
                $this->data['comment'] = $comment['data'];
            } else {
                $this->data['comment'] = [];
            }
        } else {
            return abort(404, 'Page not found');
        }

        // Latest Post
        $latest_post = $this->http_get($this->url_api() . 'content?page=1&perpage=5');
        if ($latest_post && $latest_post['status'] == 200) {
            $this->data['latest_post'] = $latest_post['data'];
        } else {
            $this->data['latest_post'] = [];
        }
        // dd($comment['data']);
        return view('components.article.detail', $this->data);
    }

    public function post_comment(Request $request)
    {
        $data = [
            'COMMENT' => $request->comment,
            'IPLOC' => $_SERVER['REMOTE_ADDR'] . "|" . $_SERVER['HTTP_USER_AGENT'],
            "FULLNAME" => $_SESSION['FULLNAME'],
            "EMAIL" => $_SESSION['EMAIL'],
            "CTIMESTAMP" => date('Y-m-d H:i:s'),
            "ARTICLE_ID" => $request->article_id,
        ];
        $http_post = $this->http_post($this->url_api() . 'comment/store', $data);
        if ($http_post['status'] == 200) {

            return redirect()->back();
        } elseif ($http_post['status'] == 422) {
            return redirect()->back()->with('backendErrors', $http_post['backendErrors']);
        } else {
            return redirect()->back();
        }
    }

    private function get_comment($id)
    {
        // Data content
        $content = $this->http_get($this->url_api() . 'comment?id=' . $id);
        if ($content && $content['status'] == 200 && count($content['data']) > 0) {
            $this->data['content'] = $content;
        } else {
            $this->data['content'] = [];
        }

        return $this->data['content'];
    }
}
