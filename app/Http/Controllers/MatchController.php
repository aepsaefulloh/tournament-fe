<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MatchController extends Controller
{
    public function index(){
        return view('components.match.index');
    }
    public function detail(){
        return view('components.match.detail');
    }
    public function upcoming(){
        return view('components.match.upcoming');
    }
}
