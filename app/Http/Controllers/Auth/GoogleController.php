<?php

namespace App\Http\Controllers\Auth;

use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GoogleController extends Controller
{
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }
    public function handleProviderCallback(Request $request)
    {
        try {
            $user_google = Socialite::driver('google')->user();
            $http_post = $this->http_post($this->url_api().'auth/google', $user_google->user);

            if ($http_post->status() == 200) {
                session_start();
                $data = $http_post['data'];
                $_SESSION = array_merge($_SESSION, $data);
                
                return redirect()->route('home');
            }else{
                return redirect()->back();
            }
        } catch (\Exception $e) {
            dd($e);
            return redirect()->route('home');
        }


    }
}
