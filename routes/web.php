<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ContentController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PromoController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\MatchController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\TournamentController;
use App\Http\Controllers\ProfileController;


use App\Http\Controllers\Auth\GoogleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::name('auth.')->prefix('auth')->group(function () {
    Route::name('redirect.')->prefix('redirect')->group(function () {
        Route::get('google', [GoogleController::class, 'redirectToProvider'])->name('google');
    });
    Route::name('callback.')->prefix('callback')->group(function () {
        Route::get('google', [GoogleController::class, 'handleProviderCallback'])->name('google');
    });
});

Route::get('/', [HomeController::class, 'index'])->name('home');

//Authenticate
Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::post('/register', [AuthController::class, 'register'])->name('register');
Route::get('/register', [AuthController::class, 'registerForm'])->name('registerForm');
Route::post('/logout', [AuthController::class, 'logout'])->middleware('authenticated')->name('logout');
Route::get('/verification-email', [AuthController::class, 'verificateEmail'])->name('verificate-email');

Route::get('/forget-password', [AuthController::class, 'forget'])->middleware('guest')->name('forget');
Route::post('/forget-password', [AuthController::class, 'forgetPost'])->middleware('guest')->name('forget');

Route::get('/reset-password', [AuthController::class, 'reset'])->middleware('guest')->name('reset');
Route::post('/reset-password', [AuthController::class, 'resetPost'])->middleware('guest')->name('reset');

// Article
Route::get('/article', [ArticleController::class, 'index'])->name('article');
Route::post('/article', [ArticleController::class, 'post_comment'])->name('article.post_comment');
Route::get('/article-detail/{id}/{title}', [ArticleController::class, 'detail'])->name('article.detail');

//Match
Route::get('/match', [MatchController::class, 'index'])->name('match');
Route::get('/match-detail', [MatchController::class, 'detail'])->name('match.detail');
Route::get('/upcoming', [MatchController::class, 'upcoming'])->name('match.upcoming');

//Tournament
Route::get('/tournament-detail/{id}', [TournamentController::class, 'detail'])->name('tournament.detail');
Route::get('/tournament-upcoming', [TournamentController::class, 'upcoming'])->name('tournament.upcoming');


//Member & Team
Route::get('/member', [MemberController::class, 'index'])->name('member');
Route::get('/profile', [MemberController::class, 'profile'])->name('member.profile');
Route::get('/team', [TeamController::class, 'index'])->name('team');
Route::get('/team-detail/{id}', [TeamController::class, 'detail'])->name('team.detail');


// About
Route::get('/about', [AboutController::class, 'index'])->name('about');

// Profile
Route::get('/profile', [ProfileController::class, 'index'])->name('profile');

// Project
Route::get('/project/category/{name}', [ProjectController::class, 'project_category'])->name('project');
Route::get('/project', [ProjectController::class, 'index'])->name('project');
Route::get('/project-detail/{id}/{title}', [ProjectController::class, 'detail'])->name('project.detail');

//Contact
Route::get('/contact', [ContactController::class, 'index'])->name('contact');

// Product
Route::get('/promo', [PromoController::class, 'index'])->name('promo');
Route::post('/add-cart', [PaymentController::class, 'add_cart'])->name('add_cart');
Route::get('/remove-cart', [PaymentController::class, 'remove_cart'])->name('remove_cart');
Route::get('/cart', [PaymentController::class, 'cart'])->name('cart');
Route::get('/order-success', [PaymentController::class, 'orderSuccess'])->name('order-success');

Route::prefix('team')->name('team.')->middleware('authenticated')->group(function () {
    Route::get('/add', [TeamController::class, 'add'])->name('add');
});
